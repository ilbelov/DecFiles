# 
#
# EventType: 11584033
#
# Descriptor: [B0 -> (D*(2010)- -> (D~0 -> K+ pi-) pi-) e+ nu_e]cc
#
# NickName: Bd_Dst+enu,D0pi+=HQET2,TightCut,tighter
# Cuts: 'LoKi::GenCutTool/TightCut'
# InsertPythonCode:
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#gen = Generation()
#gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay = "[ (Beauty) ==> ^(D~0 => ^K+ ^pi- ) ^e+ nu_e {X} {X} {X} {X} {X} {X} {X} {X} ]CC"
#tightCut.Preambulo += [
#  "from LoKiCore.functions import in_range"  ,
#  "from GaudiKernel.SystemOfUnits import GeV, MeV",
#  "piKP     = GCHILD(GP,('K+' == GABSID )) + GCHILD(GP,('pi-' == GABSID ))",
#  "piKPT     = GCHILD(GPT,('K+' == GABSID )) + GCHILD(GPT,('pi-' == GABSID ))" 
#]
#tightCut.Cuts      =    {
# '[pi+]cc'   : " ( GTHETA > 0.01 ) & ( ( GPX / GPZ ) < 0.38 ) & ( ( GPY / GPZ ) < 0.28 ) & ( ( GPX / GPZ ) > - 0.38 ) & ( ( GPY / GPZ ) > - 0.28 ) & ( GPT > 250 * MeV ) & ( GP > 2600 * MeV ) & ( GP < 200000 * MeV )" ,
# '[K-]cc'    : " ( GTHETA > 0.01 ) & ( ( GPX / GPZ ) < 0.38 ) & ( ( GPY / GPZ ) < 0.28 ) & ( ( GPX / GPZ ) > - 0.38 ) & ( ( GPY / GPZ ) > - 0.28 ) & ( GPT > 250 * MeV ) & ( GP > 2600 * MeV ) & ( GP < 200000 * MeV )" ,
# '[e+]cc'    : " ( GTHETA > 0.01 ) & ( ( GPX / GPZ ) < 0.38 ) & ( ( GPY / GPZ ) < 0.28 ) & ( ( GPX / GPZ ) > - 0.38 ) & ( ( GPY / GPZ ) > - 0.28 ) &  in_range( 2.4 , GETA, 4.0 ) & ( GP > 5500* MeV)",
# '[D~0]cc'   : " ( piKP > 15000. * MeV ) & (piKPT > 2950 * MeV)"
#    }
# EndInsertPythonCode
#
# Documentation: B -> D*+ e nu.  D* -> D0 pi, D0 -> K pi. HQET2 B decay. Tight generator level cuts. 
# Tighter cuts for the combined angular analysis of B->D*munu and B->D*enu.
# The cuts are motivated by the large B -> D*+ mu nu MC request https://indico.cern.ch/event/944960/contributions/3988560/attachments/2091830/3515170/20-08-26_svende_SL_new_mc_request.pdf
# EndDocumentation
# PhysicsWG: B2SL
# Tested: Yes
# CPUTime: < 1 min

# Responsible: Bogdan Kutsenko
# Email: bogdan.kutsenko@cern.ch
# Date: 20230704


#
Alias      MyD0         D0
Alias      MyAntiD0     anti-D0
ChargeConj MyD0         MyAntiD0
#
Alias      MyD*-        D*-
Alias      MyD*+        D*+
ChargeConj MyD*-        MyD*+
#
Decay B0sig 
   1   MyD*-        e+  nu_e         PHOTOS  HQET2 1.122 0.908 1.270 0.852 1.15;
Enddecay
CDecay anti-B0sig

Decay MyD*-
1.0       MyAntiD0   pi-                   VSS;
Enddecay
CDecay MyD*+
#
Decay MyAntiD0
  1.00   K+  pi-                           PHOTOS PHSP;
Enddecay
CDecay MyD0
#

#
End
