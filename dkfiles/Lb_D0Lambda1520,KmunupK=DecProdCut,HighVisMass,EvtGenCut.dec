# EventType: 15574078
#
# Descriptor: [Lambda_b0 -> (D0 -> K- mu+ nu_mu) (Lambda(1520)0 -> p+ K-)]cc
#
# NickName: Lb_D0Lambda1520,KmunupK=DecProdCut,HighVisMass,EvtGenCut
# Cuts: DaughtersInLHCb
#
# InsertPythonCode:
# #
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
#
# gen.SignalPlain.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/HighVisMass"
# evtgendecay.addTool(LoKi__GenCutTool ,'HighVisMass')
# evtgendecay.HighVisMass.Decay   = '[^(Lambda_b0 => (D0 => K- mu+ nu_mu) (Lambda(1520)0 => p+ K-) )]CC'
# evtgendecay.HighVisMass.Preambulo += [
#                                       "CS = LoKi.GenChild.Selector",
#                                       "visMass  = ( ( GMASS ( CS('[(Lambda_b0 => (D0 => ^K- mu+ nu_mu) (Lambda(1520)0 => p+ K-) )]CC') , CS('[(Lambda_b0 => (D0 => K- ^mu+ nu_mu) (Lambda(1520)0 => p+ K-) )]CC'), CS('[(Lambda_b0 => (D0 => K- mu+ nu_mu) (Lambda(1520)0 => ^p+ K-) )]CC'), CS('[(Lambda_b0 => (D0 => K- mu+ nu_mu) (Lambda(1520)0 => p+ ^K-) )]CC') ) ) > 4500 * MeV ) " ]
# evtgendecay.HighVisMass.Cuts    = { '[Lambda_b0]cc' : "visMass" }
#
# EndInsertPythonCode
#
# Documentation: 
# Semileptonic Lambda_b decay into D0 Lambda(1520). D0 decays to K- mu nu, L1520 to p K.
# Generator level cut applied to have a visible mass larger than 4.5 GeV.
# Semileptonic background for Lb->Lambda(1520)emu.
# EndDocumentation
#
# CPUTime: <1 min
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Dan Thompson   
# Email: dan.thompson@cern.ch
# Date: 20230705
#
Alias      MyD0          D0
Alias      Myanti-D0     anti-D0
ChargeConj MyD0          Myanti-D0
#
Alias      MyLambda(1520)0      Lambda(1520)0
Alias      Myanti-Lambda(1520)0 anti-Lambda(1520)0
ChargeConj MyLambda(1520)0      Myanti-Lambda(1520)0
#
Decay Lambda_b0sig
  1.000    MyD0  MyLambda(1520)0      PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyD0
  1.000    K-  mu+  nu_mu                 ISGW2;
Enddecay
CDecay Myanti-D0
#
Decay MyLambda(1520)0
  1.000   p+          K-        PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0
#
End
#
