# EventType: 13872600
#
# Descriptor: [B_s0  -> (D_s*- => (D_s- => mu- anti-nu_mu X) gamma) rho+]cc
#
# NickName: Bs_DsstarRhop,DsSL=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# SignalFilter = gen.SignalRepeatedHadronization.TightCut
# SignalFilter.Decay = "^( Beauty --> (Charm --> [mu+]cc ...) [pi+]cc  ...)"
# SignalFilter.Preambulo += [
#  "from GaudiKernel.SystemOfUnits import  GeV",
#  "inAcc                = in_range ( 0.005 , GTHETA , 0.400 ) &  in_range ( 1.9 , GETA , 5.1 )",
#  "muCuts               = (0 < GNINTREE ( ('mu-' == GABSID ) & (GP > 5 * GeV) &  (GPT > 1.2 * GeV)  & inAcc))",
#  ]
# SignalFilter.Cuts =  { "Beauty" : "muCuts" }
# EndInsertPythonCode
#
# Documentation: Bs -> Dsstar rho+, Dsstar -> gamma / pi0, with the Ds decaying (semi)leptonically. Require a high-pT muon.
# EndDocumentation
#
# PhysicsWG: B2SL
# Tested: Yes
# CPUTime: 3min
# Responsible: Michel De Cian
# Email: michel.de.cian@cern.ch
# Date: 20230509
#
Alias      MyD_s-     D_s-
Alias      MyD_s+     D_s+
ChargeConj MyD_s+     MyD_s-
#
Alias      MyD_s*+    D_s*+
Alias      MyD_s*-    D_s*-
ChargeConj MyD_s*+    MyD_s*-
#
Alias      MyRho+     rho+
Alias      MyRho-     rho-
ChargeConj MyRho+     MyRho-
#
Decay B_s0sig
  1.00      MyD_s*-       MyRho+         SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
Enddecay
CDecay anti-B_s0sig
#
Decay MyD_s*-
  0.942   MyD_s-  gamma                  PHOTOS VSP_PWAVE;
  0.058   MyD_s-  pi0                    PHOTOS VSS;
Enddecay
CDecay MyD_s*+
#
Decay MyD_s-
  0.0190     phi   mu-     anti-nu_mu   PHOTOS  ISGW2;
  0.0240     eta   mu-     anti-nu_mu   PHOTOS  ISGW2;
  0.0110     eta'  mu-     anti-nu_mu   PHOTOS  ISGW2;
  0.0034     K0    mu-     anti-nu_mu   PHOTOS  ISGW2; # using Ds -> K0 e nu
  0.0022     K*0   mu-     anti-nu_mu   PHOTOS  ISGW2; # using Ds -> K*0 e nu
  0.0054           mu-     anti-nu_mu   PHOTOS  SLN;	
Enddecay
CDecay MyD_s+
#
Decay MyRho+
  1.00      pi+        pi0        VSS;
Enddecay
CDecay MyRho-
#
#
End

