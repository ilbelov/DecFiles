# EventType: 16105936
#
# NickName: Ombm_OmmPipPim,OmmToL0Km,L0Toppi=TightCut,AngularCut
#
# Descriptor: [Xi_b- -> (Omega- -> (Lambda0 -> p+ pi-) K-) pi+ pi-]cc
#
# Cuts: LoKi::GenCutTool/TightCut
#
# ParticleValue: "Xi_b- 122 5132 -1.0 6.046 1.64e-012 Xi_b- 5132 0.000000e+000", "Xi_b~+ 123 -5132 1.0 6.046 1.64e-012 anti-Xi_b+ -5132 0.000000e+000"
#
#InsertPythonCode:
#
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#gen = Generation()
#gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay = "^[Xi_b- ==> (Omega- ==> (Lambda0 ==> p+ pi-) K-) pi+ pi-]CC"
#tightCut.Preambulo += [
#"from GaudiKernel.SystemOfUnits import MeV, meter",
#"InAcc = in_range ( 0.005 , GTHETA , 0.400 ) & in_range ( 1.8 , GETA , 5.2 )",
#"InAcc_DD = in_range ( 0.001 , GTHETA , 0.390 ) & in_range ( 1.8 , GETA , 7.0 )",
#"GVZ = LoKi.GenVertices.PositionZ()" ,
#"decay_position = in_range ( -1.1 * meter, GFAEVX ( GVZ, 100 * meter ), 3 * meter )",
#"good_pi = ( GP > 2500 * MeV ) & ( GPT > 200 * MeV) & InAcc",
#"good_OmmKm = ( GP > 1000 * MeV ) & ( GPT > 200 * MeV) & InAcc_DD",
#"good_L0p = ( GPT > 500 * MeV) & InAcc_DD",
#"good_L0pi = ( GPT > 100 * MeV) & InAcc_DD",
#"good_L0 = ( ( 'Lambda0' == GABSID ) & (GNINTREE( good_L0p, 1 ) > 0 ) & ( GNINTREE( good_L0pi, 1 ) > 0 ) & decay_position )",
#"good_Omm = ( ( 'Omega-' == GABSID ) & (GNINTREE( good_L0, 1 ) > 0 ) & ( GNINTREE( good_OmmKm, 1 ) > 0 ) & decay_position )",
#"good_Ombm = ( ( 'Xi_b-' == GABSID ) & (GNINTREE( good_Omm, 1 ) > 0 ) & (GNINTREE( good_pi, 1 ) > 1 )  )"
#]
#
#tightCut.Cuts = {
#'[Xi_b-]cc' : "good_Ombm"
#}
#
#EndInsertPythonCode
#
# Documentation: Xibm redefined for Omegabm, OmegabmToOmmPipPim, Omegam forced to Lambda0 K-, Lambda0 forced into p+ pi-; TightCut
# EndDocumentation
#
# PhysicsWG: BnoC
#
# Tested: Yes
# Responsible: Miroslav Saur
# Email: miroslav.saur@cern.ch
# Date: 20240125
# CPUTime: 2 min
#
#
Alias      MyOmega     Omega-
Alias      Myanti-Omega anti-Omega+
ChargeConj Myanti-Omega MyOmega
#
Alias      MyLambda      Lambda0
Alias      Myanti-Lambda anti-Lambda0
ChargeConj Myanti-Lambda MyLambda
# 
Decay Xi_b-sig 
1.000    MyOmega          pi+ pi-      PHSP;
Enddecay
CDecay anti-Xi_b+sig
#
Decay MyOmega
  1.000     MyLambda   K-      HELAMP   0.551  0.0  0.834  0.0;
Enddecay
CDecay Myanti-Omega
#
Decay MyLambda
  1.000     p+   pi-             HELAMP   0.936   0.0   0.351   0.0;
Enddecay
CDecay Myanti-Lambda
#
End

