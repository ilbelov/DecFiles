# EventType: 15166096
#
# Descriptor: [Lambda_b0 => (D0 -> K+ K- pi+ pi-) p+ pi-]cc
#
# NickName: Lb_D0ppi,KKpipi=LbsqDalitz,DAmpGen,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut 
#
#InsertPythonCode:
##
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#Generation().SignalPlain.addTool( LoKi__GenCutTool,'TightCut')
#tightCut = Generation().SignalPlain.TightCut
#tightCut.Decay = '^[Beauty => ^(D0 => ^K+ ^K- ^pi+ ^pi-) ^p+ ^pi-]CC'
#tightCut.Preambulo += [
#    'from GaudiKernel.SystemOfUnits import millimeter',
#    'inAcc        = in_range(0.005, GTHETA, 0.400) & in_range(1.8, GETA, 5.2)',
#    'inY          = in_range(1.8, GY, 4.8)',
#    'goodH        = (GP > 1000 * MeV) & (GPT > 98 * MeV) & inAcc',
#    'goodB0       = (GP > 25000 * MeV) & (GPT > 1500 * MeV) & (GTIME > 0.05 * millimeter) & inY',
#    'goodD0       = (GP > 10000 * MeV) & (GPT > 500 * MeV) & inY',
#    'goodBDaugPi  = (GNINTREE( ("pi+" == GABSID) & (GP > 2000 * MeV), 1) > 0.5)',
#    'goodBDaugP   = (GNINTREE( ("p+" == GABSID) & (GP > 2000 * MeV), 1) > 0.5)'
#]
#tightCut.Cuts = {
#    '[pi+]cc'         : 'goodH',
#    '[K+]cc'          : 'goodH',
#    '[p+]cc'          : 'goodH',
#    'Beauty'          : 'goodB0 & goodBDaugPi & goodBDaugP', 
#    '[D0]cc'          : 'goodD0'
#    }
#EndInsertPythonCode
#
# Documentation: Lb decay with flat square Dalitz model, D0 decay following AmpGen LHCb model DtoKKpipi_v2, tight cuts 
# EndDocumentation
#
# CPUTime: 2min
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Yuya Shimizu 
# Email: yuya.shimizu@cern.ch 
# Date: 20230130
#

Alias MyD0        D0
Alias Myanti-D0   anti-D0
ChargeConj MyD0   Myanti-D0


Decay Lambda_b0sig
  1.0   MyD0        p+         pi-  FLATSQDALITZ;
Enddecay
CDecay anti-Lambda_b0sig

Decay MyD0
  1.0  K+    K-    pi+    pi-      LbAmpGen DtoKKpipi_v2;
Enddecay
CDecay Myanti-D0

End
