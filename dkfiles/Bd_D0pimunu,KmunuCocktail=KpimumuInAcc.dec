# EventType: 11574442
#
# Descriptor: {[[B0]nos -> (D~0 -> K+ mu- anti-nu_mu) pi- mu+ nu_mu]cc, [[B0]os -> (D0 -> K- mu+ nu_mu) pi+ mu- anti-nu_mu]cc}
#
# NickName: Bd_D0pimunu,KmunuCocktail=KpimumuInAcc
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
# kpimumuInAcc = Generation().SignalRepeatedHadronization.TightCut
# kpimumuInAcc.Decay = '[B0 ==> K+ pi- ^mu+ ^mu- nu_mu nu_mu~ {X} {X} {X} {X} {X}]CC'
# kpimumuInAcc.Preambulo += [
#     'inAcc        = (in_range(0.010, GTHETA, 0.400))',
#     'onePionInAcc = (GNINTREE( ("pi-"==GABSID) & inAcc) >= 1)',
#     'oneKaonInAcc = (GNINTREE( ("K-"==GABSID) & inAcc) >= 1)'
#     ]
# kpimumuInAcc.Cuts = {
#     '[B0]cc'   : 'onePionInAcc & oneKaonInAcc',
#     '[mu+]cc'   : 'inAcc'
#     }
#
# EndInsertPythonCode
#
# Documentation: B0 -> D0 pi mu nu decays with D0 -> K mu nu
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: H. Tilquin
# Email: hanae.tilquin@cern.ch
# Date: 20211221
# CPUTime: < 1 min
#
Alias       MyD0          D0
Alias       Myanti-D0     anti-D0
ChargeConj  MyD0          Myanti-D0
#
Alias       MyD*0         D*0
Alias       Myanti-D*0    anti-D*0
ChargeConj  MyD*0         Myanti-D*0
#
Alias       MyD_1+        D_1+
Alias       MyD_1-        D_1-
ChargeConj  MyD_1+        MyD_1-
#
Alias       MyD'_1+       D'_1+
Alias       MyD'_1-       D'_1-
ChargeConj  MyD'_1+       MyD'_1-
#
Alias       MyD_0*+       D_0*+
Alias       MyD_0*-       D_0*-
ChargeConj  MyD_0*+       MyD_0*-
#
Alias       MyD_2*+       D_2*+
Alias       MyD_2*-       D_2*-
ChargeConj  MyD_2*+       MyD_2*-
#
Alias       MyK*+         K*+
Alias       MyK*-         K*-
ChargeConj  MyK*+         MyK*-
#
Alias       Mytau+        tau+
Alias       Mytau-        tau-
ChargeConj  Mytau+        Mytau-
#
Decay B0sig
  0.0058   Myanti-D*0    pi-  mu+     nu_mu   GOITY_ROBERTS;
  0.0058   Myanti-D*0    pi-  Mytau+  nu_tau  PHSP;
  0.0019   Myanti-D0     pi-  mu+     nu_mu   GOITY_ROBERTS;
  0.0019   Myanti-D0     pi-  Mytau+  nu_tau  PHSP;
  0.0028   MyD_1-             mu+     nu_mu   ISGW2;
  0.0019   MyD_2*-            mu+     nu_mu   ISGW2;
  0.0030   MyD_0*-            mu+     nu_mu   ISGW2;
  0.0031   MyD'_1-            mu+     nu_mu   ISGW2;
  0.0028   MyD_1-             Mytau+  nu_tau  ISGW2;
  0.0019   MyD_2*-            Mytau+  nu_tau  ISGW2;
  0.0030   MyD_0*-            Mytau+  nu_tau  ISGW2;
  0.0031   MyD'_1-            Mytau+  nu_tau  ISGW2;
Enddecay
CDecay anti-B0sig
#
Decay MyD*0
  0.647         MyD0        pi0                 VSS; 
  0.353         MyD0        gamma               VSP_PWAVE; 
Enddecay
CDecay Myanti-D*0
#
Decay MyD0
  0.0341        K-              mu+    nu_mu    ISGW2;
  0.0189        MyK*-           mu+    nu_mu    ISGW2;
  0.0160        K-         pi0  mu+    nu_mu    PHSP;
Enddecay
CDecay Myanti-D0
#
Decay MyD_0*+
  1.000         MyD0            pi+             PHSP;
Enddecay
CDecay MyD_0*-
#
Decay MyD_2*+
  0.600         MyD0            pi+             TSS;
  0.300         MyD*0           pi+             TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
Enddecay
CDecay MyD_2*-
#
Decay MyD_1+
  1.000         MyD*0           pi+             VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay
CDecay MyD_1-
#
Decay MyD'_1+
  1.000         MyD*0           pi+             VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyD'_1-
#
Decay MyK*+
  1.000         K+              pi0             VSS;
Enddecay
CDecay MyK*-
#
Decay Mytau-
  1.000         mu-    nu_tau   anti-nu_mu      TAULNUNU;
Enddecay
CDecay Mytau+
#
End

