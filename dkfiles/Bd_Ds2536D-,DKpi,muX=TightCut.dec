# EventType: 11896090
# NickName: Bd_Ds2536D-,DKpi,muX=TightCut
# Descriptor: [B0 -> (D- -> mu- nu_mu~ X) (D_s1(2536)+ -> (D+ -> K- pi+ pi+) pi- K+)]cc
#
# Cuts: LoKi::GenCutTool/TightCut
#
#InsertPythonCode:
##
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
#tightCut = Generation().SignalRepeatedHadronization.TightCut
#tightCut.Decay = '^[B0 -> ^(D- => ^mu- nu_mu~ {X} {X} {X} {X} {X} {X}) (D_s1(2536)+ => ^(D+ => ^K- ^pi+ ^pi+) ^pi- ^K+) {gamma} ]CC'
#tightCut.Preambulo += [
#    'GVZ = LoKi.GenVertices.PositionZ()',
#    'from GaudiKernel.SystemOfUnits import millimeter',
#    'inAcc        = (in_range(0.005, GTHETA, 0.400) & in_range ( 1.8 , GETA , 5.2))',
#    'goodK        = in_range( 2. * GeV , GP , 200 * GeV) & (GPT >  80 * MeV)',
#    'goodPi       = in_range( 2. * GeV , GP , 200 * GeV) & (GPT >  80 * MeV)',
#]
#tightCut.Cuts = {
#    '[K+]cc'        : 'inAcc & goodK',
#    '[pi+]cc'       : 'inAcc & goodPi', 
#    '[mu+]cc'       : 'in_range( 0.010 , GTHETA , 0.400 ) & (GP > 2500 * MeV)', 
#    '[B0]cc'        : 'GALL', 
#    '[D+]cc'        : 'GALL', 
#    }
#EndInsertPythonCode
#
# Documentation: 
#    Decay file for B0 => D- D_s1(2536)+
#    D_s1(2536) decays to D+ pi- K+ via phase space. 
#    D+ resonant decay forced. 
#    Semileptonic D- decay. 
# EndDocumentation
# CPUTime: < 1 min
# 
# Date:   20230707
# Responsible: Anton Poluektov
# Email: anton.poluektov@cern.ch
# PhysicsWG: B2SL
# Tested: Yes

Alias My_D+    D+
Alias My_D-    D-
ChargeConj My_D+   My_D- 

Alias My_OtherD-    D-
Alias My_OtherD+    D+
ChargeConj  My_OtherD-    My_OtherD+

Alias My_Ds2536        D'_s1+
Alias My_anti-Ds2536   D'_s1-
ChargeConj My_Ds2536     My_anti-Ds2536

#
Decay My_Ds2536
  1.0 My_OtherD+ pi- K+ PHSP;
Enddecay
CDecay My_anti-Ds2536
#
Decay My_D-
  0.055000000 K*0     mu-     anti-nu_mu                      PHOTOS  ISGW2; 
  0.094000000 K0      mu-     anti-nu_mu                      PHOTOS  ISGW2; 
  0.002773020 K_10    mu-     anti-nu_mu                      PHOTOS  ISGW2; 
  0.002927076 K_2*0   mu-     anti-nu_mu                      PHOTOS  ISGW2; 
  0.003312218 pi0     mu-     anti-nu_mu                      PHOTOS  ISGW2; 
  0.002002736 eta     mu-     anti-nu_mu                      PHOTOS  ISGW2; 
  0.000385142 eta'    mu-     anti-nu_mu                      PHOTOS  ISGW2; 
  0.002500000 rho0    mu-     anti-nu_mu                      PHOTOS  ISGW2; 
  0.002156793 omega   mu-     anti-nu_mu                      PHOTOS  ISGW2; 
  ##Split this contribution into resonant, nonresonant
  ##0.039000000 K+      pi-     mu-     anti-nu_mu              PHOTOS   PHSP; 
  0.002000000 K+      pi-     mu-     anti-nu_mu              PHOTOS   PHSP;
  
  0.001078397 K0      pi0     mu-     anti-nu_mu              PHOTOS   PHSP; 
  0.000382000 mu-     anti-nu_mu                              PHOTOS   SLN; 
Enddecay
CDecay My_D+
#
Decay My_OtherD+
  1.0   K-  pi+  pi+   D_DALITZ;
Enddecay
CDecay My_OtherD-
#
Decay B0sig
  1.000 My_D- My_Ds2536 PHSP;
Enddecay
CDecay anti-B0sig

End
