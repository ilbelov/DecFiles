# EventType: 37123001
#
# Descriptor: [ K+ => pi+ e- e+ ]cc
#
# NickName: K+_pi+e-e+=TightCut
#
# Cuts: 'LoKi::GenCutTool/TightCut'
#
# Documentation: Forces a K+ to pi+ e- e+
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[^(K+ => ^pi+ ^e- ^e+)]CC'
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import meter, millimeter, GeV" ,
#     "inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )         " ,
#     "inEta        =  in_range ( 1.95  , GETA   , 5.050 )         " ,
#     "fastTrack    =  ( GP  > 1.5 * GeV )   " ,
#     "goodTrack    =  inEta & fastTrack & inAcc                   " ,     
#     "GVX = LoKi.GenVertices.PositionX()                          " ,
#     "GVY = LoKi.GenVertices.PositionY()                          " ,
#     "GVZ = LoKi.GenVertices.PositionZ()                          " ,
#     "vx      = GFAEVX ( GVX, 100 * meter )                       " ,    
#     "vy      = GFAEVX ( GVY, 100 * meter )                       " ,
#     "rho2    = vx**2 + vy**2                                     " ,
#     "rhoK  =  rho2 < ( 2500 * millimeter )**2                      " , 
#     "decay = in_range ( -0.1 * meter,  GFAEVX ( GVZ, 100 * meter ),  2.27 * meter ) ",
#     "positivez = ( GPZ > 0 * MeV )                                         ",
# ]
# tightCut.Cuts      =    {
#     '[K+]cc'  : ' decay & rhoK & positivez ',
#     '[pi+]cc' : ' goodTrack ',
#     '[e+]cc' : ' goodTrack '
#                         }
# EndInsertPythonCode
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Lukas Calefice
# Email: lukas.calefice@cern.ch
# Date: 20210112
# CPUTime: 2 min
#
Decay       K+sig
  1.000      pi+ e- e+     PHSP;
Enddecay
CDecay K-sig
#
End
