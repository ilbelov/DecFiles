# EventType: 23263023
#
# Descriptor: [D_s+ -> K- K+ pi+]cc
#
# NickName: Ds+_K-K+pi+=res,TightCut,ACPKKCuts
#
# Cuts: LoKi::GenCutTool/TightCut
#
#Documentation: Ds+ forced into K- K+ pi+ with Dalitz decay model. K-K+ selected in order to have m(K-K+) around m(phi). Tight cuts to match the selection for ACP(D02KK) measurement with prompt decays. 
# EndDocumentation 

# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# signal     = Generation().SignalPlain
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# cuts   = signal.TightCut
# cuts.Decay     = '^[ D_s+ => ^K- ^K+ ^pi+]CC'
# cuts.Preambulo += [
# 'from GaudiKernel.SystemOfUnits import MeV, GeV, micrometer ',
# 'import math',
# 'inAcc      = in_range (2 , GETA ,  4.5 ) & (GPT > 240 * MeV) & (GP > 900 * MeV)',
# 'inAcc_pip      = in_range (2 , GETA ,  4.5 ) & (GPT > 0.75 * GeV) & (GP > 4.9 * GeV)',
# 'inAcc_Dp      = ( GVEV & (GTIME > 0.15 * 122.95 * micrometer ) )',
#  'CombCuts  =  ( GCOUNT( ( ("pi+" == GABSID ) | ("K+" == GABSID ) ) & (GPT > 0.9 * GeV), HepMC.descendants ) >= 1 ) & ( GCOUNT( ( ("pi+" == GABSID ) | ("K+" == GABSID ) ) & (GPT > 0.3 * GeV), HepMC.descendants ) >= 2 ) & in_range (2 , GETA ,   4.5) & ( GPT > 2.9 *GeV )',
# 'PhiMass      = ( ( GMASS("K-" == GID ,"K+" == GID) > 969.455 *MeV) & ( GMASS("K-" == GID, "K+" == GID) < 1069.455 *MeV) )', #phi in Dalitz
# ]		 
# cuts.Cuts      =    {
#     '[K-]cc'  : ' inAcc',
#     '[pi+]cc'  : ' inAcc_pip',
#     '[D_s+]cc'   : 'inAcc_Dp & PhiMass & CombCuts'
#                         }
# EndInsertPythonCode
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Serena Maccolini
# Email: serena.maccolini@cern.ch
# Date: 20200919
# CPUTime: < 1 min
#
Decay D_s+sig
  1.000        K-        K+        pi+          D_DALITZ;
Enddecay
CDecay D_s-sig
#
End
