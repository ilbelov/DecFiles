# EventType: 15694000
#
# Descriptor: [[Lambda_b0] ==> (Lambda_c+ ==> p+ K- pi+) (D_s- -> tau- anti-nu_tau)]cc
#
# NickName: Lb_LcDs,pKpi,taumunu=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: Lb to Lc Ds, with Ds->tau nu_tau, background mode for Lb to Lc mu nu.
# EndDocumentation
## InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# Generation().SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut  = Generation().SignalPlain.TightCut
# tightCut.Decay = "[ Lambda_b0 ==> (Lambda_c+ ==> ^p+ ^K- ^pi+ {X} {X} {X} {X}) (D_s- ==> (tau-=> mu- nu_mu~ nu_tau) nu_tau~) {X} {X} {X} {X}]CC"
# tightCut.Preambulo += [
# "from LoKiCore.functions import in_range",
# "from GaudiKernel.SystemOfUnits import GeV, MeV",
# "pKpiP      = GCHILD(GP,1) + GCHILD(GP,2) + GCHILD(GP,3)" ,
# "pKpiPT     = GCHILD(GPT,1) + GCHILD(GPT,2) + GCHILD(GPT,3)",
#  ]
# tightCut.Cuts      =    {
#'[p+]cc'   : "in_range( 0.010 , GTHETA , 0.400 ) & ( GPT > 250 * MeV ) & ( GP > 6000 * MeV )",
#'[K-]cc'   : "in_range( 0.010 , GTHETA , 0.400 ) & ( GPT > 250 * MeV ) & ( GP > 1500 * MeV )",
#'[pi+]cc'  : "in_range( 0.010 , GTHETA , 0.400 ) & ( GPT > 250 * MeV ) & ( GP > 1500 * MeV )",
#'[mu-]cc'  : "in_range( 0.010 , GTHETA , 0.400 )",
#'[Lambda_c+]cc' : "( (pKpiPT/3) > 500 * MeV)"
# }  
# EndInsertPythonCode
#
# CPUTime: <1min
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Scott Ely
# Email: scott.ely@cern.ch
# Date: 20211109
#
Alias		MyD_s+			D_s+
Alias		MyD_s-			D_s-
ChargeConj	MyD_s+			MyD_s-
#
Alias		MyLambda_c+		Lambda_c+
Alias		MyLambda_c-		anti-Lambda_c-
ChargeConj	MyLambda_c+		MyLambda_c-
#
Alias		MyK*0          K*0
Alias		Myanti-K*0     anti-K*0
ChargeConj 	MyK*0          Myanti-K*0
#
Alias		MyLambda(1520)0		   Lambda(1520)0
Alias		Myanti-Lambda(1520)0	   anti-Lambda(1520)0
ChargeConj	MyLambda(1520)0		   Myanti-Lambda(1520)0
#
Alias		Mytau+		tau+
Alias		Mytau-		tau-
ChargeConj	Mytau+		Mytau-
#
Decay Lambda_b0sig
 1.0000		MyLambda_c+	MyD_s-		PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyLambda_c+
  0.02800         p+      K-      pi+          PHSP;
  0.01065         p+      Myanti-K*0           PHSP;
  0.00860         Delta++ K-                   PHSP;
  0.00414         MyLambda(1520)0 pi+          PHSP;
Enddecay
CDecay MyLambda_c-
#
Decay MyD_s+
 1.0000	     Mytau+	nu_tau		SLN;
Enddecay
CDecay MyD_s-
#
Decay Mytau-
  0.1736        mu-     anti-nu_mu      nu_tau  TAULNUNU;
Enddecay
CDecay Mytau+
#
Decay MyK*0
  0.6657	K+	pi-	VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyLambda(1520)0
  0.2300	p+	K-	PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0
#
End
