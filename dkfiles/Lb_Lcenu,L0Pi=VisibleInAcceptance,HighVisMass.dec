# EventType: 15584101
# 
# Descriptor: {[ Lambda_b0 -> (Lambda_c+ -> (Lambda0 -> p+ pi-) pi+)  anti-nu_e e-]cc}
# 
# NickName: Lb_Lcenu,L0Pi=VisibleInAcceptance,HighVisMass
# Cuts: LoKi::GenCutTool/HighVisMass
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool(LoKi__GenCutTool ,'HighVisMass')
# #
# tightCut = gen.SignalPlain.HighVisMass
# tightCut.Decay   = '[^(Lambda_b0 => ^(Lambda_c+ => ^(Lambda0 => ^p+ ^pi-) ^pi+) ^e- ^nu_e~)]CC'
# tightCut.Cuts    =    {
#     '[p+]cc'        : "inAcc",
#     '[pi-]cc'       : "inAcc",
#     '[pi+]cc'       : "inAcc",
#     '[e-]cc'        : "inAcc",
#     '[Lambda_b0]cc' : "visMass" }
# tightCut.Preambulo += [
#     "inAcc   = in_range ( 0.005 , GTHETA , 0.400 ) " ,
#     "visMass  = ( ( GMASS ( 'pi+' == GABSID , 'e-' == GABSID, 'p+' == GABSID, 'pi+' == GABSID ) ) > 4500 * MeV ) " ]
# EndInsertPythonCode
#
# Documentation:Semi-leptonic Lambda B decay into Lc e Nu. Lc is forced to Lambda0 pi+, and Lambda0 forced to pi+ pi-.
# Generator level cut applied to have a visible mass larger than 4.5 GeV, for Lb->Lemu and Lb->Lee.
# EndDocumentation
#
# CPUTime: 10 min
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Mick Mulder   
# Email: mick.mulder@cern.ch
# Date: 20170807
# EndDocumentation
#
Alias      MyLambda0      Lambda0
Alias      Myanti-Lambda0 anti-Lambda0
ChargeConj MyLambda0      Myanti-Lambda0
#
Alias MyLambda_c+       Lambda_c+
Alias Myanti-Lambda_c-  anti-Lambda_c-
ChargeConj MyLambda_c+  Myanti-Lambda_c-
#
###
Decay Lambda_b0sig
  1.000    MyLambda_c+        e-  anti-nu_e     PHOTOS   BaryonPCR  1 1 1 1;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyLambda_c+
  1.000   MyLambda0 pi+	       PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
Decay MyLambda0
  1.000   p+          pi-    PHSP;
Enddecay
#
Decay Myanti-Lambda0
  1.000   anti-p-    pi+     PHSP;
Enddecay 
#
End
