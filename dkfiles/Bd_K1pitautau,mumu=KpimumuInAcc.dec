# EventType: 11616050
#
# NickName: Bd_K1pitautau,mumu=KpimumuInAcc
# Descriptor: {[[B0]nos -> (K_1+ -> K+ pi0 pi0) pi- (tau+ -> mu+ nu_mu anti-nu_tau) (tau- -> mu- anti-nu_mu nu_tau)]cc, [[B0]os -> (K_1- -> K- pi0 pi0) pi+ (tau+ -> mu+ nu_mu anti-nu_tau) (tau- -> mu- anti-nu_mu nu_tau)]cc}
# 
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
# kpimumuInAcc = Generation().SignalRepeatedHadronization.TightCut
# kpimumuInAcc.Decay = '[B0 ==> ^mu+ ^mu- nu_mu nu_mu~ K+ pi- pi0 pi0 nu_tau nu_tau~]CC'
# kpimumuInAcc.Preambulo += [
#     'inAcc        = (in_range(0.010, GTHETA, 0.400))',
#     'onePionInAcc = (GNINTREE( ("pi-"==GABSID) & inAcc) >= 1)',
#     'oneKaonInAcc = (GNINTREE( ("K-"==GABSID) & inAcc) >= 1)'
#     ]
# kpimumuInAcc.Cuts = {
#     '[B0]cc'    : 'onePionInAcc & oneKaonInAcc',
#     '[mu+]cc'   : 'inAcc'
#     }
#
# EndInsertPythonCode
#
# Documentation: B0 -> K1 pi tau tau decays with K1 -> K pi0 pi0 decays. K, pi, mu, mu in acceptance
# EndDocumentation
#
# CPUTime: < 1min
#
# PhysicsWG: RD 
# Tested: Yes
# Responsible: H. Tilquin
# Email: hanae.tilquin@cern.ch
# Date: 20221026
#
Alias      MyK1_1270_+   K_1+ 
Alias      MyK1_1270_-   K_1-
ChargeConj MyK1_1270_+   MyK1_1270_-
#
Alias      MyK1_1400_+   K'_1+
Alias      MyK1_1400_-   K'_1-
ChargeConj MyK1_1400_+   MyK1_1400_-
#
Alias      MyK*1430_+    K_0*+
Alias      MyK*1430_-    K_0*-
ChargeConj MyK*1430_+    MyK*1430_-
#
Alias      MyK*+         K*+
Alias      MyK*-         K*-
ChargeConj MyK*+         MyK*-
#
Alias      Mytau+        tau+
Alias      Mytau-        tau-
ChargeConj Mytau+        Mytau-
#
Decay B0sig
  0.500    MyK1_1270_+  pi-   Mytau+     Mytau-        PHSP;
  0.500    MyK1_1400_+  pi-   Mytau+     Mytau-        PHSP;
Enddecay
CDecay anti-B0sig
#
Decay MyK1_1270_+
  0.28     MyK*1430_+   pi0                            VSS;
  0.21     MyK*+        pi0                            VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.30     K+           pi0   pi0                      PHSP;
Enddecay
CDecay MyK1_1270_-
#
Decay MyK1_1400_+
  0.94     MyK*+        pi0                            VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.02     K+           pi0   pi0                      PHSP;
Enddecay
CDecay MyK1_1400_-
#
Decay MyK*1430_+
  1.00     K+           pi0                            PHSP;
Enddecay
CDecay MyK*1430_-
#
Decay MyK*+
  1.000    K+           pi0                            VSS;
Enddecay
CDecay MyK*-
#
Decay Mytau+
  1.000    mu+          nu_mu anti-nu_tau              TAULNUNU;
Enddecay
CDecay Mytau-
#
End
#

