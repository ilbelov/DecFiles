# EventType: 12145451
#
# Descriptor:  [B+ ->  (psi(2S) -> (J/psi(1S) -> mu+ mu-) pi+ pi-) ( K*(892)+ -> K+ pi0) ]cc
#
# NickName: Bu_psi2SKst,Jpsipipi,mm=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = '^[ B+ =>  (psi(2S) => ^(J/psi(1S) => ^mu+ ^mu- ) ^pi+ ^pi-)  (K*(892)+ => ^K+ (pi0 -> ^gamma ^gamma)) ]CC'
# tightCut.Cuts      =    {
#     'gamma'     : ' goodGamma ' ,
#     '[mu+]cc'   : ' goodMuon  ' ,
#     '[K+]cc'    : ' goodKaon  ' ,
#     '[pi+]cc'   : ' goodPion  ' ,
#     'J/psi(1S)' : ' goodJPsi   ' ,
#     '[B+]cc'    : ' goodB     '}
# tightCut.Preambulo += [  "GY = LoKi.GenParticles.Rapidity()" ]
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import ns',
#     'from GaudiKernel.PhysicalConstants import c_light',
#     'inAcc     = in_range ( 0.005 , GTHETA , 0.400 ) ' ,
#     'inEcalX   = abs ( GPX / GPZ ) < 4.5 / 12.5      ' ,
#     'inEcalY   = abs ( GPY / GPZ ) < 3.5 / 12.5      ' ,
#     'inEcalHole= (abs ( GPX / GPZ ) < 0.25 / 12.5) & (abs ( GPY / GPY ) < 0.25 / 12.5)' ,
#     'goodMuon  = ( GPT > 500  * MeV ) & ( GP > 6 * GeV )     & inAcc   ' ,
#     'goodKaon  = ( GPT > 150  * MeV )                        & inAcc   ' ,
#     'goodPion  = ( GPT > 150  * MeV )                        & inAcc   ' ,
#     'goodGamma = ( 0 < GPZ ) & ( 150 * MeV < GPT ) & inEcalX & inEcalY & ~inEcalHole' ,
#     'goodJPsi   = ( GPT > 1000  * MeV ) & in_range( 1.8 , GY , 4.5 )    ' ,
#     'goodB     = ( GCTAU > 0.1e-3 * ns * c_light )']
#
# EndInsertPythonCode

#
# Documentation:  Normalisation channel for X(3872)/X(3915)->Jpsi Omega. With tight cuts.
# EndDocumentation
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Lorenzo Capriotti
# Email: lorenzo.capriotti@cern.ch
# Date: 20201209
# CPUTime: 2min
#
Define Hp 0.159
Define Hz 0.775
Define Hm 0.612
Define pHp 1.563
Define pHz 0.0
Define pHm 2.712
#
#
Alias      Mypsi(2S)  psi(2S)
Alias      MyJ/psi    J/psi
Alias      MyK*+      K*+
Alias      MyK*-      K*-
Alias      Mypi0      pi0
#
ChargeConj Mypsi(2S)  Mypsi(2S)
ChargeConj MyJ/psi    MyJ/psi
ChargeConj MyK*+      MyK*-
ChargeConj Mypi0      Mypi0
#
Decay B+sig
1.000     Mypsi(2S)  MyK*+            SVV_HELAMP Hp pHp Hz pHz Hm pHm;
Enddecay
Decay B-sig
1.000     Mypsi(2S)  MyK*-               SVV_HELAMP Hm pHm Hz pHz Hp pHp;
Enddecay
#
Decay Mypsi(2S)
  1.000         MyJ/psi         pi+   pi-         VVPIPI;
Enddecay
# 
Decay MyJ/psi
  1.000         mu+         mu-     PHOTOS     VLL;
Enddecay
#
Decay MyK*+
  1.000        K+      Mypi0                VSS;
Enddecay
#
Decay MyK*-
  1.000        K-      Mypi0                VSS;
Enddecay
#
Decay Mypi0
1.000    gamma    gamma     PHSP;
Enddecay
#
End
