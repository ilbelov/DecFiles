# EventType: 14643230
#
# Descriptor: [B_c+ => (Charmonium -> (J/psi(1S) -> e+ e-) gamma) mu+ nu_mu]cc
#
# NickName: Bc_CharmoniumMuNu,Jpsigamma,ee=BcVegPy,ffWang,TightCut
#
# Production: BcVegPy
#
# Cuts: LoKi::GenCutToolWithDecay/TightCut
#
# Documentation: Decay Bc -> chi_cX mu nu with chi_cX -> Jpsi gamma and Jpsi -> e+ e-. Charged leptons in acceptance and high momentum muon required.
# EndDocumentation
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutToolWithDecay
# gen = Generation().Special
# gen.addTool(LoKi__GenCutToolWithDecay, "TightCut")
# gen.CutTool = "LoKi::GenCutToolWithDecay/TightCut"
# SignalFilter = gen.TightCut
# SignalFilter.SignalPID = "B_c+"
# SignalFilter.Decay = "^( [B_c+ --> (J/psi(1S) => e+ e-) mu+ ...]CC )"
# SignalFilter.Preambulo += [
#    "from GaudiKernel.SystemOfUnits import GeV",
#    "inAcc = in_range( 0.005, GTHETA, 0.400 )",
#    "muCuts = ( 0 < GNINTREE ( ('mu+' == GABSID ) & ( GP > 5 * GeV ) & ( GPT > 1.2 * GeV ) & inAcc ) )",
#    "epCuts = ( 0 < GNINTREE ( ('e+' == GID ) & inAcc ) )",
#    "emCuts = ( 0 < GNINTREE ( ('e-' == GID ) & inAcc ) )",
#    "allcuts = ( muCuts & epCuts & emCuts )"
#   ]
# SignalFilter.Cuts = { "[B_c+]cc" : "allcuts" }
# EndInsertPythonCode
#
# PhysicsWG: B2SL
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Fabian Christoph Glaser
# Email: fabian.christoph.glaser@cern.ch
# Date: 20230627
#
Alias       Mychi_c0    chi_c0
ChargeConj  Mychi_c0    Mychi_c0
#
Alias       Mychi_c1    chi_c1
ChargeConj  Mychi_c1    Mychi_c1
#
Alias       Mychi_c2    chi_c2
ChargeConj  Mychi_c2    Mychi_c2
#
Alias       MyJpsi      J/psi
ChargeConj  MyJpsi      MyJpsi
#
Decay B_c+sig 
    #predicted branching fractions of Bc -> chi_cX mu nu are multiplied by branching fractions of chi_cX -> J/psi gamma
    0.020720      Mychi_c0    mu+     nu_mu       PHOTOS BC_SMN 3;
    0.343000      Mychi_c1    mu+     nu_mu       PHOTOS BC_VMN 3;
    0.317300      Mychi_c2    mu+     nu_mu       PHOTOS BC_TMN 3;
Enddecay
CDecay B_c-sig
#
Decay Mychi_c0
    1.0000      MyJpsi      gamma       SVP_HELAMP 1.0 0.0 1.0 0.0;
Enddecay
#
Decay Mychi_c1
    1.0000      MyJpsi      gamma       VVP 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
#
Decay Mychi_c2
    1.0000      MyJpsi      gamma       PHSP;
Enddecay
#
Decay MyJpsi
    1.0000      e+          e-          PHOTOS VLL;
Enddecay
#
End
#
