# EventType: 12165503
# 
# Descriptor: [ B+ -> (Lambda~0 -> p~- pi+) p+ (D*(2007)~0 -> (D~0 -> K+ pi-) (pi0 -> gamma gamma) ) ]cc 
# 
# NickName: Bu_LambdabarpDst0bar,D0barpi0,Kpi=HELAMP,TightCut
#
# Cuts: DaughtersInLHCb
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = "[B+ ==> (Lambda~0 => ^p~- ^pi+) ^p+ (D*(2007)~0 => (D~0 => ^K+ ^pi-) (pi0 => gamma gamma))]CC"
# tightCut.Preambulo += [
# "from GaudiKernel.SystemOfUnits import MeV",
# "InAcc = in_range ( 0.005 , GTHETA , 0.400 )",
# "goodKpi  = ( GP > 1000 * MeV ) & ( GPT > 100 * MeV ) & InAcc",
# "goodp    = ( GP > 5000 * MeV ) & ( GPT > 200 * MeV ) & InAcc"
# ]
# tightCut.Cuts = {
# '[pi+]cc' : "goodKpi",
# '[K+]cc'  : "goodKpi",
# '[p+]cc'  : "goodp"
# }
#
# EndInsertPythonCode
# 
# Documentation:
#                 Lambda0 forced into p pi.
#                 Lambda0 -> p pi helicity amplitude from 2019 combination in PRL 123, 182301.
#                 All charged final state tracks are required to be within the LHCb acceptance.
# EndDocumentation
#
# PhysicsWG: BnoC
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Akar Simon
# Email: simon.akar@cern.ch
# Date: 20230512
#
#

Alias       MyD*0         D*0
Alias       Myanti-D*0    anti-D*0
ChargeConj  MyD*0         Myanti-D*0
Alias       MyD0          D0
Alias       Myanti-D0     anti-D0
ChargeConj  MyD0          Myanti-D0
Alias       MyLambda      Lambda0
Alias       MyantiLambda  anti-Lambda0
ChargeConj  MyLambda      MyantiLambda
Alias       mypi0         pi0
ChargeConj  mypi0         mypi0


#
Decay B+sig
  1.000     MyantiLambda  p+ Myanti-D*0           PHSP; 
Enddecay
CDecay B-sig
#
Decay MyLambda
  1.000     p+            pi-                    HELAMP 0.9276 0.0 0.3735 0.0;
Enddecay
CDecay MyantiLambda
#
Decay Myanti-D*0
1.000    Myanti-D0    mypi0                      VSS;
Enddecay
CDecay MyD*0
#
Decay MyD0
  1.000     K-            pi+                    PHSP;
Enddecay
CDecay Myanti-D0

Decay mypi0
  1.000   gamma          gamma        PHSP;
Enddecay


#
End
#
