# EventType: 12165526
#
# Descriptor: [B+ -> (anti-D0 -> (K*(892)0 -> (KS0 -> pi+ pi-)  (pi0 -> gamma gamma)) (rho(770)0 -> pi- pi+)) pi+]cc
# 
# NickName: Bu_D0Pi,Kst0Rho0,Kspipipi0=TightCut,PHSP
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '^[B+ -> ^(D~0 ==> ^(K*(892)0 -> ^(KS0 ==> ^pi+ ^pi-) ^(pi0 ==> ^gamma ^gamma)) ^(rho(770)0 -> ^pi- ^pi+)) ^pi+]CC'
# tightCut.Preambulo += [
#     'GVZ = LoKi.GenVertices.PositionZ() ' ,
#     'from GaudiKernel.SystemOfUnits import millimeter',
#     'inAcc        = (in_range (0.005, GTHETA, 0.400))',
#     'inEcalX   = abs ( GPX / GPZ ) < 4.5 / 12.5      ' ,
#     'inEcalY   = abs ( GPY / GPZ ) < 3.5 / 12.5      ' ,
#     'goodB        = (GP > 7500 * MeV) & (GPT > 1500 * MeV) & (GTIME > 0.105 * millimeter)',
#     'goodD        = (GP > 4000 * MeV) & (GPT > 400 * MeV)',
#     'goodKS       = (GNINTREE(("KS0"==GABSID) & (GP >  4000 * MeV) & (GPT >  400 * MeV) & (GFAEVX(abs(GVZ), 0) < 2500.0 * millimeter))>0.5)',
#     'goodDaugPi  = (GNINTREE (("pi+" == GABSID) & (GP > 750 * MeV) & inAcc, 1) > 1.5)',
#     'goodKsDaugPi = (GNINTREE (("pi+" == GABSID) & (GP > 500 * MeV) & inAcc, 1) > 1.5)',
#     'goodBachPi   = (GNINTREE (("pi+" == GABSID) & (GP > 5000 * MeV) & (GPT > 500 * MeV) & inAcc, 1) > 0.5)',
#     'goodPi0      =  (GNINTREE( ("pi0"==GABSID) & (GP > 750 * MeV) & (GPT > 300 * MeV) & inAcc, 1) > 0.5)',
#    'goodPi0Gamma = (GNINTREE( ("gamma"==GABSID) & (GP > 750 * MeV) & (GPT > 400 * MeV) & inEcalX  & inEcalY  & inAcc, 1) > 1.5)'
# ]
# tightCut.Cuts      =    {
#     '[B+]cc'         : 'goodB  & goodBachPi',
#     '[D0]cc'         : 'goodD',
#     '[rho(770)0]cc'   :   'goodDaugPi  ',
#     '[K*(892)0]cc'   :   'goodKS & goodPi0 ',
#     '[KS0]cc'        : 'goodKsDaugPi',
#     '[pi0]cc'        : 'goodPi0Gamma'
#     }
# EndInsertPythonCode
#
# Documentation: B decays to D0pi, D0 decays to KSpipipi0 throught K*0 and rho0 resonance PHSP, KS decays to pi+pi-, pi0 decays to gamma gamma 
# all decay products , and including gammas, in acceptance and tight cuts
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: 4 min
# Responsible: Vincent Tisserand
# Email: tisserav@lapp.in2p3.fr
# Date: 20170407
#
Alias MyD0 D0
Alias Myanti-D0 anti-D0
Alias MyK_S0 K_S0
Alias Mypi0 pi0
Alias MyRho0 rho0
Alias MyK*0      K*0
Alias Myanti-K*0 anti-K*0
ChargeConj MyK*0 Myanti-K*0
ChargeConj MyD0 Myanti-D0
ChargeConj MyK_S0 MyK_S0
##
Decay B+sig
1.000     Myanti-D0  pi+  PHSP;
Enddecay
CDecay B-sig
#
Decay MyD0
1.000     Myanti-K*0  MyRho0                          PHSP;
Enddecay
CDecay Myanti-D0
#
Decay MyK*0
1.000 MyK_S0 Mypi0       VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyRho0
1.000 pi- pi+  VSS;
Enddecay
#
Decay MyK_S0
1.000     pi+  pi-  PHSP;
Enddecay
#
Decay Mypi0
1.0000  gamma gamma                         PHSP;
Enddecay
#
End
