# EventType: 11576060
#
# Descriptor: [B~0 -> (Lambda_c+ -> p+ K- mu+ nu_mu) p~- pi+ pi-]cc
#
# NickName: Bd_Lcpipip,pKmunu=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
#
# gen.SignalRepeatedHadronization.addTool(LoKi__GenCutTool ,'TightCut')
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay   = '[^(Beauty ==> ^(Lambda_c+ ==> p+ K- mu+ nu_mu) pi- pi+ p~-)]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, centimeter',
#     'inAcc    = in_range ( 1.80 , GETA , 5.10 )' ,
#     'goodProtonFromLc = ( GPT > 0.45 * GeV ) & in_range ( 8 * GeV, GP, 160 * GeV ) & inAcc ' ,
#     'goodProtonFromB0 = ( GPT > 0.1 * GeV ) & in_range ( 8 * GeV, GP, 160 * GeV ) & inAcc ' ,
#     'goodKaon = ( GPT > 0.45 * GeV ) & ( GP > 2.9 * GeV )  & inAcc ' ,
#     'goodMuonFromLc = ( GPT > 0.45 * GeV ) & ( GP > 2.9 * GeV ) & inAcc ' ,
#     'goodPionFromB0 = ( GPT > 0.10 * GeV ) & ( GP > 1.0 * GeV ) & inAcc ' ,
#     'goodLc = (GNINTREE(("p+" == GABSID) & goodProtonFromLc) > 0) & (GNINTREE(("K+" == GABSID) & goodKaon) > 0) & (GNINTREE(("mu+" == GABSID) & goodMuonFromLc) > 0 )',
#     'goodB0 = (GFAEVX(GVZ, 0) - GFAPVX(GVZ, 0) > 0.8 * millimeter) & (GPT > 0.19 * GeV) & (GNINTREE(("pi+"==GABSID) & goodPionFromB0, HepMC.children) > 1) & (GNINTREE(("p+"==GABSID) & goodProtonFromB0, HepMC.children) > 0)'
#     ]
# tightCut.Cuts      =    {
#     '[B0]cc'  : 'goodB0',
#     '[Lambda_c+]cc'  : 'goodLc'
#     }
# EndInsertPythonCode
#
# Documentation: This is the decay file for the decay B0 -> Lambda_c+ pi- pi+ anti-p-. 
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Yangjie Su
# Email: yangjie.su@cern.ch
# Date: 20230103
#CPUTime:  16 min
#

#Define Lambda_c
Alias MyLambda_c+ Lambda_c+
Alias Myanti-Lambda_c- anti-Lambda_c-
ChargeConj MyLambda_c+ Myanti-Lambda_c-

#
Decay anti-B0sig
  1.0     MyLambda_c+  pi- pi+ anti-p-          PHSP;
Enddecay
CDecay B0sig
#
Decay MyLambda_c+
  1.000         p+      K-      mu+ nu_mu         PHSP;
Enddecay
CDecay Myanti-Lambda_c-

End
