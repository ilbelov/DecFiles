# EventType: 15466010
# Descriptor: [Lambda_b0 -> (Lambda_c(2625)+ -> (Lambda_c+ -> p+ K- pi+) pi+ pi-)  pi- pi+ pi-]cc
#
# NickName: Lb_Lc2625pipipi,Lcpipi,pKpi=cocktail,DecProdCut
#
# Cuts: DaughtersInLHCb
# CutsOptions: NeutralThetaMin 0. NeutralThetaMax 10.
#
# CPUTime: 1min
#
# Documentation: Lb->Lc3pi decay taking into account both resonances for the Lc
# and the 3pi part. Two modes using Lc* resonances are also added.
# This decfile is designed to provide the most suitable production for the
# normalisation channel of Lb->Lctaunu, tau->3pi(pi0)nu_tau.
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Guy Wormser
# Email: wormser@lal.in2p3.fr
# Date: 20180109
#


Alias      MyLambda_c+             Lambda_c+
Alias      Myanti-Lambda_c-        anti-Lambda_c-
ChargeConj MyLambda_c+             Myanti-Lambda_c-
#
Alias      MyLambda_c(2593)+       Lambda_c(2593)+
Alias      Myanti-Lambda_c(2593)-  anti-Lambda_c(2593)-
ChargeConj MyLambda_c(2593)+       Myanti-Lambda_c(2593)-
#
Alias      MyLambda_c(2625)+       Lambda_c(2625)+
Alias      Myanti-Lambda_c(2625)-  anti-Lambda_c(2625)-
ChargeConj MyLambda_c(2625)+       Myanti-Lambda_c(2625)-
#
Alias      MyK*0                   K*0
Alias      Myanti-K*0              anti-K*0
ChargeConj MyK*0                   Myanti-K*0
#
Alias      MyLambda(1520)0         Lambda(1520)0
Alias      Myanti-Lambda(1520)0    anti-Lambda(1520)0
ChargeConj MyLambda(1520)0         Myanti-Lambda(1520)0
#
Alias      Mya_1-                  a_1-
Alias      Mya_1+                  a_1+
ChargeConj Mya_1+                  Mya_1-
#
Alias      Myf_2                   f_2
ChargeConj Myf_2                   Myf_2
#
#
Decay Lambda_b0sig
  0.63    MyLambda_c(2625)+        Mya_1-         PHSP;
  0.135   MyLambda_c(2625)+        rho0  pi-      PHSP;
  0.09    MyLambda_c(2625)+        Myf_2 pi-      PHSP;
  0.045   MyLambda_c(2625)+        pi-   pi+  pi- PHSP;

Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyLambda_c+
# Lc->pKpi:
  0.02800 p+                 K-      pi+   PHSP;
  0.016   p+                 Myanti-K*0    PHSP;
  0.00860 Delta++            K-            PHSP;
  0.00414 MyLambda(1520)0    pi+           PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
Decay MyLambda_c(2625)+
  1.000  MyLambda_c+   pi+    pi-          PHSP;
Enddecay
CDecay Myanti-Lambda_c(2625)-
#
Decay MyLambda_c(2593)+
  1.000  MyLambda_c+   pi+    pi-          PHSP;
Enddecay
CDecay Myanti-Lambda_c(2593)-
#
Decay MyK*0
  0.6667 K+                  pi-           VSS;
Enddecay
CDecay Myanti-K*0
#
Decay Mya_1+
  1.000   rho0               pi+           VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Mya_1-
#
Decay Myf_2
  1.000  pi+                 pi-           TSS ;
Enddecay
#
End
