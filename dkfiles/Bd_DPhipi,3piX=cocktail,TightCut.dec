# EventType: 11366000
# 
# Descriptor: [B_0 -> D- (phi -> K- K+) pi+]cc
# 
# NickName: Bd_DPhipi,3piX=cocktail,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool , 'TightCut')
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay = "[Beauty ==> ^(Charm) (phi(1020) ==> ^K+ ^K-) ^pi+]CC"
# tightCut.Preambulo += [ "from LoKiCore.functions import in_range",
#   "from GaudiKernel.SystemOfUnits import  GeV, MeV, mrad",
#   'goodcharm   = (GNINTREE(("pi+"==GABSID) & ( GPT > 250 * MeV ) & in_range( 0.010 , GTHETA , 0.400 ) & (GNINTREE(("K0" == GABSID), HepMC.ancestors)==0), HepMC.descendants) > 2.5)',]
# tightCut.Cuts      =    {
# '[pi+]cc'  : " in_range( 0.010 , GTHETA , 0.400 ) & ( GPT > 250 * MeV )",
# '[K-]cc'   : "  in_range( 0.010 , GTHETA , 0.400 )  & ( GPT > 250 * MeV )",
# '[D+]cc'   : 'goodcharm',
#    }
# EndInsertPythonCode
#
# Documentation: B0 -> D- phi pi with D -> 3piX, with tight cuts. Includes resonances in D decay
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# CPUTime: <1min
# Responsible: Matt Rudolph
# Email: matthew.scott.rudolph@cern.ch
# Date: 20221010
#
#
Alias      MyD-     D-
Alias      MyD+     D+
ChargeConj MyD+     MyD-
#
Alias      MyPhi      phi
ChargeConj MyPhi      MyPhi
#
# K*0 -> K+ pi-
Alias MyK*0_f K*0
Alias Myanti-K*0_f anti-K*0
ChargeConj MyK*0_f Myanti-K*0_f
#
# K*- -> KS0 pi-
Alias MyK*-_f K*-
Alias MyK*+_f K*+
ChargeConj MyK*-_f MyK*+_f

# a1+ -> rho0 pi+
Alias Mya_1+ a_1+
Alias Mya_1- a_1-
ChargeConj Mya_1+ Mya_1-

# eta -> 2piX
Alias Myeta_2piX eta
ChargeConj Myeta_2piX Myeta_2piX

# eta !-> 2piX
Alias Myeta_not2piX eta
ChargeConj Myeta_not2piX Myeta_not2piX

# omega -> 2piX
Alias Myomega_2piX omega
ChargeConj Myomega_2piX Myomega_2piX

# eta' -> 2piX
Alias Myeta'_2piX eta'
ChargeConj Myeta'_2piX Myeta'_2piX

# K’_10 -> KS0bar pi+ pi-
Alias MyK'_10 K'_10
Alias Myanti-K'_10 anti-K'_10
ChargeConj MyK'_10 Myanti-K'_10

#
Decay B0sig
  1.000    MyD-     MyPhi    pi+    PHSP;
Enddecay
CDecay anti-B0sig
#

#
Decay MyD+

0.71 K0 pi+ Myeta_2piX PHSP;  # Gamma_71 (PDG 2021 update) * 2 = 2.62, * B(eta -> 2piX)
0.16 K0 pi+ Myeta'_2piX PHSP; # Gamma_72 (PDG 2021 update) * 2 = 0.38, * B(eta' -> 2piX)

#Breakup of Gamma_74 (PDG 2021 update) D+ -> Ks0 2pi+ pi-, broken up in 2008 PDG. Inclusive = 3.1% (which gets *2)

#Gamma_68(2008) D+ -> Ks0 a1+ = 1.8% has agreement b/w ANJOS and COFFMAN
#Gamma_69(2008) D+ -> K1(1400) pi+ has disagreement. ANJOS does not see it, COFFMAN does
#Gamma_70(2008) D+ -> K*(892)- pi+ pi+ also has disagreement. ANJOS sees it, COFFMAN does not
#Gamma_71(2008) D+ -> Ks0 rho0 pi+ is almost completely dominated by Gamma_68, D+ -> Ks0 a1+, a1+ -> rho0 pi+. So this is ignored
#Gamma_72(2008) D+ -> Ks0 rho0 pi+ 3body is tiny and has a 100% error. So this is ignored
#Gamma_73(2008) D+ -> Ks0 2pi+ pi- NR = 0.36 is seen by both ANJOS and COFFMAN. 

#So we assign 3.1 - 1.8 - 0.36 = 0.94 broken up equally among Gamma_69 and Gamma_70

3.6 Mya_1+ K0  SVS;        #Gamma_68 (2008 PDG) * 2
0.94 Myanti-K'_10 pi+ SVS; #(Total - Gamma_68 - Gamma_73)*0.5 (2008 PDG) * 2, K1(1400) -> KS0bar pi+ pi-
0.94 MyK*-_f pi+ pi+ PHSP; #(Total - Gamma_68 - Gamma_73)*0.5 (2008 PDG) * 2, K*- -> KS0 pi- *
0.72 K0 pi+ pi+ pi- PHSP;  #Gamma_73 (2008 PDG) * 2
#

0.036 K- pi+ pi+ Myeta_2piX PHSP; #Gamma_75 (PDG 2021 update) = 0.135 * B(eta -> 1piX)
0.066 K0 pi+ pi0 Myeta_2piX PHSP; #Gamma_76 (PDG 2021 update)*2 = 0.244 * B(eta -> 2piX)

#Breakup of Gamma_77 (PDG 2021 update) D+ -> K- 3pi+ pi-

#Rescaling slightly so that sum is equal to inclusive number 0.57 (interference effects)
#Following breakup according to Table 2 in arxiv:0211056
#Since 3 body K* rho pi is dominated by 2 body a1+ K* mode, I am simply replacing Gamma_79 by Gamma_80
#but with Gamma_79’s number

0.123 Myanti-K*0_f pi+ pi+ pi- PHSP;                           #Gamma_78 (PDG 2021 update) rescaled, K*0 forced to K- pi+
0.235 Mya_1+ Myanti-K*0_f  SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0; #Gamma_79 (PDG 2021 update) rescaled, a1 forced to rho0 pi+, K*0 forced to K- pi+
0.176 K- rho0 pi+ pi+ PHSP;                                    #Gamma_82 (PDG 2021 update) rescaled
0.041 K- pi+ pi+ pi+ pi- PHSP;                                 #Gamma_83 (PDG 2021 update) rescaled
# 

0.58 Mya_1+ pi0 SVS;                               #Half of Gamma_101 (PDG 2021 update), a1 forced to rho0 pi+
0.58 rho+ rho0 SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0; #Other half of Gamma_101 (PDG 2021 update)

0.166 pi+ pi+ pi+ pi- pi- PHSP;        #Gamma_102 (PDG 2021 update)
0.102 Myeta_2piX pi+ PHSP;             #Gamma_103 (PDG 2021 update) = 0.377 * B(eta->2piX) 
0.056 Myeta_2piX pi+ pi0 PHSP;         #Gamma_104 (PDG 2021 update) = 0.205 * B(eta->2piX)
0.341 eta pi+ pi+ pi- PHSP;            #Gamma_105 (PDG 2021 update), all eta decays
0.087 Myeta_2piX pi+ pi0 pi0 PHSP;     #Gamma_106 (PDG 2021 update) = 0.320 * B(eta->2piX)
0.02 Myeta_2piX Myeta_2piX pi+ PHSP;  #Gamma_107 (PDG 2021 update) = 0.296 * B(eta->2piX) * B(eta->2piX)
0.12 Myeta_not2piX Myeta_2piX pi+ PHSP; #Gamma_107 (PDG 2021 update) = 0.296 * (1-B(eta->2piX)) * B(eta->2piX) * 2
0.354 Myomega_2piX pi+ pi0 PHSP;       #Gamma_109 (PDG 2021 update) = 0.390 * B(omega->2piX)
0.214 Myeta'_2piX pi+  PHSP;           #Gamma_110 (PDG 2021 update) = 0.497 * B(eta'->2piX)
0.069 Myeta'_2piX pi+ pi0 PHSP;        #Gamma_111 (PDG 2021 update) = 0.16  * B(eta'->2piX)

Enddecay
CDecay MyD-
#
###############################################
# See DDKst0, 3piX=cocktail files for more info
###############################################
#
#
Decay MyK*0_f
1.0 K+ pi- VSS;
Enddecay
CDecay Myanti-K*0_f

#
Decay MyK*-_f
1.0 K_S0 pi- VSS;
Enddecay
CDecay MyK*+_f

#
Decay Mya_1+
1.0 rho0 pi+ VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Mya_1-

#
Decay Myeta_2piX
0.2292 pi- pi+ pi0 ETA_DALITZ;
0.0422 gamma pi- pi+ PHSP;
Enddecay

#
Decay Myeta_not2piX #Copied from DECAY.DEC
0.3931 gamma gamma PHSP; #[Reconstructed PDG2011]
0.3257 pi0 pi0 pi0 PHSP; #[Reconstructed PDG2011]
Enddecay

#
Decay Myeta'_2piX
0.115 pi+ pi- Myeta_2piX PHSP; #B(eta'->pi+pi- eta)=0.425 * B(eta -> 2piX)
0.06  pi0 pi0 Myeta_2piX PHSP; #B(eta'->pi0 pi0 eta)=0.224 * B(eta -> 2piX)
0.295 rho0 gamma SVP_HELAMP 1.0 0.0 1.0 0.0;
0.023 Myomega_2piX gamma SVP_HELAMP 1.0 0.0 1.0 0.0; #B(eta' -> omega gamma) = 0.0252 * B(omega -> 2piX)
Enddecay

#
Decay Myomega_2piX
0.892 pi- pi+ pi0 OMEGA_DALITZ;
0.0153 pi- pi+ VSS;
Enddecay

#
Decay MyK'_10
1.0 K_S0 pi+ pi- PHSP;
Enddecay
CDecay Myanti-K'_10

#
Decay MyPhi
  1.000    K+         K-              VSS;
Enddecay
End
