# EventType: 13102263
#
# Descriptor: [B_s0 -> (phi(1020) -> K+ K-) gamma]cc
#
# NickName: Bs_Phigamma=PHSP,TightCut  
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: 1 min
#
# InsertPythonCode:
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
#
# gen = Generation()
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
#
# gen.SignalRepeatedHadronization.setProp('MaxNumberOfRepetitions', 5000)
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[Beauty => (phi(1020) => ^K+ ^K-) ^gamma]CC'
# tightCut.Cuts      =    {
#     '[K+]cc'         : ' inAcc' , 
#     'gamma'          : ' goodPhoton'
#     }
#
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "inAcc         = in_range ( 0.005 , GTHETA , 0.400 ) " ,
#     "inEcalX       =   abs ( GPX / GPZ ) < 4.5  / 12.5",
#     "inEcalY       =   abs ( GPY / GPZ ) < 3.5  / 12.5",
#     "inEcalHole    = ( abs ( GPX / GPZ ) < 0.25 / 12.5 ) & ( abs ( GPY / GPZ ) < 0.25 / 12.5 )",
#     "InEcal        = inEcalX & inEcalY & ~inEcalHole ",
#     "goodPhoton    = ( GPT > 1.8 * GeV ) & InEcal"
#     ]
#
# EndInsertPythonCode
#
# Documentation: for PhiG, gamma PT > 1.8 GeV, inAcceptance
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Biplab Dey
# Email:  biplab.dey@.cern.ch
# Date: 20201119
#
Alias      MyPhi   phi
ChargeConj MyPhi   MyPhi
#
Decay B_s0sig
 1.0   MyPhi gamma PHSP;
Enddecay
CDecay anti-B_s0sig
#
Decay MyPhi
  1.000        K+        K-      VSS;
Enddecay
#
End
