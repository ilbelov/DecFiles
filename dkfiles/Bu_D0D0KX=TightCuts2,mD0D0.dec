# EventType: 12495403
# 
# Descriptor:  [B+ ==> D0 D~0 K+ { pi+ } { pi- } { pi0 } { pi0 } { gamma } { gamma } ]CC 
# 
# NickName: Bu_D0D0KX=TightCuts2,mD0D0 
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: The decay B+ -> ( D0D~0, D*0D~0, D0D~*0, D*0D~*0, D*+D*- ) K+ 
#                with D0-> K- pi+ or D0-> K- pi+ pi+ pi- with the tight generator cuts. 
#                At most one D0 -> K- pi+ pi+ pi- decay is allowed.  
#                The D0 -> K- pi+ pi+ pi-  decay model is taken from EPJ. C78 (2018) 443. 
#                The mass of D0D~0 system is required to be less than 3.8GeV. 
#                To be efficient some cuts are applied via EvtGenDecayWithCut tool 
#                - many thanks to Michael Wilkinson for inspiring  examples. 
# EndDocumentation
#
# Sample: SignalRepeatedHadronization
#
# InsertPythonCode:
# 
# from Configurables import LoKi__GenCutTool, ToolSvc, EvtGenDecayWithCutTool
# from Gauss.Configuration import *
#
# ToolSvc().addTool ( EvtGenDecayWithCutTool )
# evtgen = ToolSvc().EvtGenDecayWithCutTool  
# evtgen.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgen.CutTool   = "LoKi::GenCutTool/SmallDD"
# evtgen.addTool( LoKi__GenCutTool , 'SmallDD' )
# small_DD = evtgen.SmallDD 
# small_DD.Decay      = ' ^[B+ ==> D0 D~0 K+ { pi+} { pi- } { pi0 } { pi0 } ]CC '
# small_DD.Preambulo += [ 
#     "from GaudiKernel.SystemOfUnits import GeV "                  ,  
#     "mass_DD        = GMASS    ( 'D0' == GID , 'D~0' ==  GID ) "  ,
#     "K3pi           = GDECTREE ( '[D0 ==> K- pi+ pi+ pi-]CC' ) "  , 
#     "N_K3pi         = GNINTREE ( K3pi ) "                         , 
#     ]
# small_DD.Cuts       = { 
#    '[B+]cc' : '( N_K3pi < 2 ) & ( mass_DD < 3.8 * GeV )' 
# }
#
# generation = Generation()
# signal     = generation.SignalRepeatedHadronization
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# signal.DecayTool = "EvtGenDecayWithCutTool"
# 
# tightCut            = signal.TightCut
# tightCut.Decay      = '^[B+ ==> ^D0 ^D~0 ^K+ { pi+ } { pi- } { pi0} { pi0 } ]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer, MeV, GeV',
#     'inAcc          =  in_range ( 0.005 , GTHETA , 0.400 ) ' ,
#     'inEta          =  in_range ( 1.95  , GETA   , 5.050 ) ' ,
#     'good_track     =  inAcc & inEta & ( GPT > 190 * MeV ) & in_range ( 3 * GeV , GP , 200 * GeV ) ' ,
#     "kaon           =  'K-'  == GABSID " , 
#     "pion           =  'pi-' == GABSID " ,  
#     "good_D0_1      =  GDECTREE ('[D0  => K- pi+]CC'         ) & ( 1 == GNINTREE ( kaon & good_track ) ) & ( 1 == GNINTREE ( pion & good_track ) ) " , 
#     "good_D0_2      =  GDECTREE ('[D0 ==> K- pi+ pi+ pi-]CC' ) & ( 1 == GNINTREE ( kaon & good_track ) ) & ( 3 == GNINTREE ( pion & good_track ) ) " , 
#     "good_D0        =  good_D0_1 | good_D0_2 ", 
# ]
# tightCut.Cuts       =    {
#     '[K+]cc'        : 'good_track'  ,
#     '[D0]cc'        : 'in_range ( 1.9 , GY , 4.6) &  good_D0 ' ,
#     '[B+]cc'        : 'in_range ( 1.9 , GY , 4.6) ' ,
#     }
# # Generator efficiency histos:
# tightCut.XAxis = ( "GPT/GeV" , 0.0 , 25.0 , 25 )
# tightCut.YAxis = ( "GY"      , 2.0 ,  4.5 , 10 )
# EndInsertPythonCode
#
# PhysicsWG:   Onia 
# Tested:      Yes
# Responsible: Vanya BELYAEV
# Email:       Ivan.Belyaev@cern.ch
# Date:        20201110
# CPUTime:     2min


Alias           My-D0           D0
Alias           My-anti-D0      anti-D0
ChargeConj      My-D0           My-anti-D0

Alias           My-D*0          D*0
Alias           My-anti-D*0     anti-D*0
ChargeConj      My-D*0          My-anti-D*0

Alias           My-D*+          D*+
Alias           My-D*-          D*-
ChargeConj      My-D*+          My-D*-

Decay B+sig
  0.25          My-D0  My-anti-D*0 K+  PHSP ;
  0.25          My-D*0 My-anti-D0  K+  PHSP ;
  0.10          My-D0  My-anti-D0  K+  PHSP ;
  0.20          My-D*0 My-anti-D*0 K+  PHSP ;
  0.20          My-D*+ My-D*-      K+  PHSP ;
Enddecay
CDecay B-sig

Decay My-D0
  0.333         K-     pi+            PHSP ; 
  0.667         K-     pi+ pi+ pi-    LbAmpGen DtoKpipipi_v2 ; 
Enddecay
CDecay My-anti-D0

Decay My-D*0
  0.353         My-D0  gamma          VSP_PWAVE ; 
  0.647         My-D0  pi0            VSS ; 
Enddecay
CDecay My-anti-D*0

Decay My-D*+
  1.000         My-D0  pi+            VSS ; 
Enddecay
CDecay My-D*-

#
End
#

