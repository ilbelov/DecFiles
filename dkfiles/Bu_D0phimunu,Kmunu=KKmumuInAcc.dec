# EventType: 12575043
#
# Descriptor: [B+ -> (D~0 -> K+ mu- anti-nu_mu) (phi(1020) -> K- K+) mu+ nu_mu]cc
#
# NickName: Bu_D0phimunu,Kmunu=KKmumuInAcc

# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
# kkmumuInAcc = Generation().SignalRepeatedHadronization.TightCut
# kkmumuInAcc.Decay = '[^(B+ ==> K+ K- ^mu+ ^mu- nu_mu nu_mu~ {X} {X})]CC'
# kkmumuInAcc.Preambulo += [
#     'inAcc        = (in_range(0.01, GTHETA, 0.400))',
#     'twoKaonsInAcc = (GNINTREE( ("K+"==GID) & inAcc) >= 1) & (GNINTREE( ("K-"==GID) & inAcc) >= 1)'
#     ]
# kkmumuInAcc.Cuts = {
#     '[mu+]cc'   : 'inAcc',
#     '[B+]cc'   : 'twoKaonsInAcc'
#     }
#
# EndInsertPythonCode
#
#
# Documentation: B+ -> anti-D0 phi mu+ nu_mu decays, with D0 -> K- mu+ nu_mu. KKmumu in acceptance.
# EndDocumentation
#
# CPUTime: < 1 min
# PhysicsWG: RD
# Tested: Yes
# Responsible: H. Tilquin
# Email: hanae.tilquin@cern.ch
# Date: 20211012
#
Alias       MyPhi      phi
ChargeConj  MyPhi      MyPhi
#
Alias       MyD0       D0
Alias       Myanti-D0  anti-D0
ChargeConj  MyD0       Myanti-D0
#
Alias       MyK*+      K*+
Alias       MyK*-      K*-
ChargeConj  MyK*+      MyK*-
#
Decay B+sig
  1.000     Myanti-D0  MyPhi      mu+     nu_mu    PHSP;
Enddecay
CDecay B-sig
#
Decay MyD0
  0.341     K-                    mu+     nu_mu    ISGW2;
  0.189     MyK*-                 mu+     nu_mu    ISGW2;
  0.160     K-         pi0        mu+     nu_mu    PHSP;
Enddecay
CDecay Myanti-D0
#
Decay MyPhi
  1.000     K+         K-                          VSS;
Enddecay
#
Decay MyK*+
  1.000     K+         pi0                         VSS;
Enddecay
CDecay MyK*-
#
End
