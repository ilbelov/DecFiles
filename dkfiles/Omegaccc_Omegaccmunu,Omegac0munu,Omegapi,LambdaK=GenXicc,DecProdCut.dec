# EventType:  26576150
#
# Descriptor: [Xi_cc++ -> (Omega_cc+ -> (Omega_c0 -> (Omega- -> (Lambda0 -> p+ pi-) K-) pi+) mu+ nu_mu) mu+ nu_mu ]cc
#
# NickName: Omegaccc_Omegaccmunu,Omegac0munu,Omegapi,LambdaK=GenXicc,DecProdCut
#
# Production: GenXicc
#
# Cuts: XiccDaughtersInLHCbAndWithMinPT
#
# CutsOptions: MinXiccPT 2500*MeV MinDaughterPT 200*MeV
#
# CPUTime: < 1 min
#
# ParticleValue: "Xi_cc++ 506 4422 2.0 4.700 1.60e-13 Xi_cc++ 4422 0.0", "Xi_cc~-- 507 -4422 -2.0 4.700 1.60e-13 anti-Xi_cc-- -4422 0.0", "Omega_cc+ 510 4432 1.0 3.738 1.60e-13 Omega_cc+ 4432 0.0", "Omega_cc~- 511 -4432 -1.0 3.738 1.60e-13 anti-Omega_cc- -4432 0.0", "Omega_c0 104 4332 0.0 2.6952 2.68e-13 Omega_c0 4332 0.0", "Omega_c~0 105 -4332 0.0 2.6952 2.68e-13 anti-Omega_c0 -4332 0.0"
#
# Documentation: Omegaccc decay to Omegacc mu+ nu, Omegacc decay to Omegac0 mu+ nu, Omegac0 decay to Omega- mu+ nu
# all daughters of Omegaccc are required to be in the acceptance of LHCb and with minimum PT 200 MeV
# Omegac0 is required to be generated with the lifetime of 268fs
# Omegaccc is required to be generated with the lifetime of 160fs and mass of 3738 MeV
# Omegaccc PT is required to be larger than 2500 MeV.
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Hang Yin
# Email: hyin@cern.ch 
# Date: 20201217
#
Alias      MyOmega_cc        Omega_cc+
Alias      Myanti-Omega_cc   anti-Omega_cc-
ChargeConj MyOmega_cc        Myanti-Omega_cc 
Alias      MyOmega_c         Omega_c0
Alias      Myanti-Omega_c    anti-Omega_c0
ChargeConj MyOmega_c         Myanti-Omega_c 
Alias      MyOmega           Omega-
Alias      Myanti-Omega      anti-Omega+
ChargeConj MyOmega           Myanti-Omega
Alias      MyLambda          Lambda0
Alias      Myanti-Lambda     anti-Lambda0
ChargeConj Myanti-Lambda     MyLambda 
#
Decay Xi_cc++sig
 1.000    MyOmega_cc     mu+ nu_mu          PHSP;
Enddecay
CDecay anti-Xi_cc--sig
#
Decay MyOmega_cc 
 1.000    MyOmega_c     mu+ nu_mu           PHSP;
Enddecay
CDecay Myanti-Omega_cc 
#
Decay MyOmega_c 
 1.000    MyOmega       pi+                PHSP;
Enddecay
CDecay Myanti-Omega_c 
#
Decay MyOmega
 1.000    MyLambda      K-                  PHSP;
Enddecay
CDecay Myanti-Omega
#
Decay MyLambda 
 1.000    p+            pi-                 PHSP;
Enddecay
CDecay Myanti-Lambda
#
End
#
