# EventType: 16107132
#
# NickName: Xibm_XimKpKmPipPim,ppi=TightCut,AngularCut
#
# Descriptor: [Xi_b- -> (Xi- -> (Lambda0 -> p+ pi-) pi-) K+ K- pi+ pi-]cc
#
# Cuts: DaughtersInLHCb
#
#InsertPythonCode:
#
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#gen = Generation()
#gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay = "^[Xi_b- ==> (Xi- (Lambda0 ==> p+ pi-) pi-) pi+ pi+ pi- pi-]CC"
#tightCut.Preambulo += [
#"from GaudiKernel.SystemOfUnits import MeV, centimeter",
#"InAcc = in_range ( 0.005 , GTHETA , 0.400 ) & in_range ( 1.8 , GETA , 5.2 )",
#"good_pi = ( GP > 2500 * MeV ) & ( GPT > 200 * MeV) & InAcc",
#"good_XimPim = ( GP > 1000 * MeV ) & ( GPT > 100 * MeV) & InAcc",
#"good_k = ( GP > 2500 * MeV ) & ( GPT > 200 * MeV) & InAcc",
#"good_L0p = ( GPT > 500 * MeV) & InAcc"
#"good_L0pi = ( GPT > 100 * MeV) & InAcc",
#"good_L0 = ( ( 'Lambda0' == GABSID ) & (GNINTREE( good_L0p, 1 ) > 0 ) & GNINTREE( good_L0pi, 1 ) > 0 ) )"
#"good_Xim = ( ( 'Xim' == GABSID ) & (GNINTREE( good_L0, 1 ) > 0 ) & GNINTREE( good_XimPim, 1 ) > 0 ) )"
#"good_Xib = ( ( 'Xi_b-' == GABSID ) & (GNINTREE( good_Xim, 1 ) > 0 ) & (GNINTREE( good_k, 1 ) > 1 ) & (GNINTREE( good_pi, 1 ) > 1 )  )"
#]
#
#tightCut.Cuts = {
#'[Xi_b-]cc' : "good_Xib"
#}
#
#EndInsertPythonCode
#
# Documentation: Lambda0 forced into p+ pi-; TightCut
# EndDocumentation
#
# PhysicsWG: BnoC
#
# Tested: Yes
# Responsible: Miroslav Saur
# Email: miroslav.saur@cern.ch
# Date: 20230822
# CPUTime: < 1 min
#
#
Alias      MyXi     Xi-
Alias      Myanti-Xi anti-Xi+
ChargeConj Myanti-Xi MyXi
#
Alias      MyLambda      Lambda0
Alias      Myanti-Lambda anti-Lambda0
ChargeConj Myanti-Lambda MyLambda
# 
Decay Xi_b-sig 
1.000    MyXi          K+ K- pi+ pi-      PHSP;
Enddecay
CDecay anti-Xi_b+sig
#
Decay MyXi
  1.000     MyLambda   pi-      HELAMP   0.551  0.0  0.834  0.0;
Enddecay
CDecay Myanti-Xi
#
Decay MyLambda
  1.000     p+   pi-             HELAMP   0.936   0.0   0.351   0.0;
Enddecay
CDecay Myanti-Lambda
#
End

