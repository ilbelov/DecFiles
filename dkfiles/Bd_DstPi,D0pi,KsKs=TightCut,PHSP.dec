# EventType: 11166105
#
# Descriptor: [B0 ->  (D*(2010)- -> (anti-D0 -> (K_S0 -> pi+ pi-) (K_S0 -> pi+ pi-))  pi-) pi+]cc
#
# NickName: Bd_DstPi,D0pi,KsKs=TightCut,PHSP
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '^[B0 -> ^(D*(2010)- -> ^(D~0 => ^(KS0 ==> ^pi+ ^pi-) ^(KS0 ==> ^pi+ ^pi-)) pi-) ^pi+]CC'
# tightCut.Preambulo += [
#     'GVZ = LoKi.GenVertices.PositionZ() ' ,
#     'from GaudiKernel.SystemOfUnits import millimeter',
#     'inAcc        = (in_range (0.010, GTHETA, 0.300))',
#     'goodB        = (GP > 7500 * MeV) & (GPT > 1500 * MeV) & (GTIME > 0.105 * millimeter)',
#     'goodD     = ( GP > 10 * GeV ) & switch(GCHILD(GPT,1) < GCHILD(GPT,2), (GCHILD(GPT,1) > 300) & (GCHILD(GPT,2) > 800), (GCHILD(GPT,1) > 800) & (GCHILD(GPT,2) > 300)) & inAcc            ' , 
#     'goodDst     = ( GP > 20 * GeV ) & (GCHILD(GTHETA,2) > 0.01) & (GCHILD(GTHETA,2) < 0.3)     ' , 
#     'goodKS   = ( GP >  4 * GeV) & inAcc & (GFAEVX( abs( GVZ  ) , 0 ) < 2500 * mm) & (GCHILD(GPT,1) > 100 * MeV) &  (GCHILD(GPT,2) > 100 * MeV) &  (GCHILD(GP,1) > 2.0 * GeV) &  (GCHILD(GP,2)  > 2.0 * GeV)        ' 
# ]
# tightCut.Cuts      =    {
#     '[D*(2010)+]cc'  : 'goodDst',
#     '[D0]cc'         : 'goodD',
#     '[KS0]cc'        : 'goodKS',
#     }
# EndInsertPythonCode
#
# Documentation: B decays to Dstarpi, Dstar decays to D0 pi, D0 decays to KSKS PHSP, KS decays to pi+pi-, with tight cuts
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: 3 min
# Responsible: Simone Stracka
# Email: simone.stracka@cern.ch
# Date: 20170610
#
Alias       myD*-       D*-
Alias       myD*+       D*+
ChargeConj  myD*+       myD*-
Alias myD0 D0
Alias myanti-D0 anti-D0
ChargeConj myD0 myanti-D0
Alias myK_S0 K_S0
ChargeConj myK_S0 myK_S0
Alias myPi0 pi0
ChargeConj myPi0  myPi0
##
Decay B0sig
  1.000     myD*-  pi+               SVS ;
Enddecay
CDecay anti-B0sig
#
Decay myD*-
1.0000    myanti-D0  pi-         VSS;
Enddecay
CDecay myD*+
#
Decay myD0
1.000     myK_S0 myK_S0  PHSP;
Enddecay
CDecay myanti-D0
#
Decay myK_S0
1.000     pi+  pi-  PHSP;
Enddecay
#
End
