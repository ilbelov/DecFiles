# EventType: 13166301
# 
# Descriptor: [ B_s0 -> (Lambda~0 -> p~- pi+) p+ (D_s*+ -> (D_s- => K- K+ pi-) gamma]cc 
# 
# NickName: Bs_LambdabarpDssm,Dsgamma,KKpi=HELAMP,TightCut
#
# Cuts: DaughtersInLHCb
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = "[B_s0 ==> (Lambda~0 => ^p~- ^pi+) ^p+ (D_s*- => (D_s- => ^K- ^K+ pi-) gamma)]CC"
# tightCut.Preambulo += [
# "from GaudiKernel.SystemOfUnits import MeV",
# "InAcc = in_range ( 0.005 , GTHETA , 0.400 )",
# "goodKpi  = ( GP > 1000 * MeV ) & ( GPT > 100 * MeV ) & InAcc",
# "goodp    = ( GP > 5000 * MeV ) & ( GPT > 200 * MeV ) & InAcc"
# ]
# tightCut.Cuts = {
# '[pi+]cc' : "goodKpi",
# '[K+]cc'  : "goodKpi",
# '[p+]cc'  : "goodp"
# }
#
# EndInsertPythonCode
# 
# Documentation:
#                 Lambda0 forced into p pi.
#                 Lambda0 -> p pi helicity amplitude from 2019 combination in PRL 123, 182301.
#                 All charged final state tracks are required to be within the LHCb acceptance.
# EndDocumentation
#
# PhysicsWG: BnoC
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Liang Sun
# Email: lsun@cern.ch
# Date: 20230301
#
#

Alias       MyDsp         D_s+
Alias       MyDsm   D_s-
ChargeConj  MyDsp       MyDsm
Alias MyDssp   D_s*+
Alias MyDssm   D_s*-
ChargeConj  MyDssp   MyDssm
Alias       MyLambda      Lambda0
Alias       MyantiLambda  anti-Lambda0
ChargeConj  MyLambda      MyantiLambda

#
Decay B_s0sig
  1.000     MyantiLambda  p+ MyDssm           PHSP; 
Enddecay
CDecay anti-B_s0sig

#
Decay MyLambda
  1.000     p+            pi-                HELAMP 0.9276 0.0 0.3735 0.0;
Enddecay
CDecay MyantiLambda

Decay MyDssm
  0.9420  MyDsm gamma  VSP_PWAVE;
Enddecay
CDecay MyDssp 

Decay MyDsm
  1.000     K-            K+        pi-        D_DALITZ;
Enddecay
CDecay MyDsp

#
End
#
