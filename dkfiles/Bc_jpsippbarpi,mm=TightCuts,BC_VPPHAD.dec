# EventType: 14175083
#
# Descriptor: [B_c+ -> (J/psi(1S) -> mu+ mu- ) p+ p~- pi+]cc
#
# NickName: Bc_jpsippbarpi,mm=TightCuts,BC_VPPHAD
#
# Production: BcVegPy
#
# Cuts: LoKi::GenCutToolWithDecay/TightCut
#
# Documentation: The baryonic Bc decay with pion, Jpsi forced into mu+ mu-, using BcVegPy generator. 
#                The decay is produced by the BC_VPPHAD model, proposed by A. Luchinsky.
#                Daughter in acceptance and TightCuts are used. The efficiency is (6.127 +- 0.265)% from Generation log 
# EndDocumentation
#
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutToolWithDecay
# from Gauss.Configuration import *
# gen = Generation()
# gen.Special.addTool ( LoKi__GenCutToolWithDecay , 'TightCut' )
# gen.Special.CutTool = 'LoKi::GenCutToolWithDecay/TightCut'
# #
# tightCut = gen.Special.TightCut
# tightCut.SignalPID = 'B_c+'
# tightCut.Decay = '[(B_c+ => (J/psi(1S) => ^mu+ ^mu- ) ^p+ ^p~- ^pi+)]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer,MeV,GeV',
#     'inAcc        = in_range ( 0.005 , GTHETA , 0.400 ) ' ,
#     'inEta        = in_range ( 1.9   , GETA   , 5.00  ) ' ,
#     'goodTrack    =  inAcc & inEta                      ' ,     
#     ]
# tightCut.Cuts     =    {
#     '[pi+]cc'        : 'goodTrack & ( GP  >   2.5 * GeV ) & ( GPT  >   130 * MeV ) ' , 
#     '[p+]cc'         : 'goodTrack & ( GP  >   8.0 * GeV ) & ( GPT  >   130 * MeV ) ' , 
#     '[mu+]cc'        : 'goodTrack & ( GP  >   3.0 * GeV ) & ( GPT  >   450 * MeV ) ' 
#     }
#
# EndInsertPythonCode
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Dmitrii Pereima
# Email: Dmitrii.Pereima@cern.ch
# Date: 20230629
# CPUTime: < 1 min
#
Alias      MyJ/psi    J/psi
ChargeConj MyJ/psi    MyJ/psi
#
Decay B_c+sig
  1.000        MyJ/psi     p+      anti-p-     pi+   BC_VPPHAD 2;
Enddecay
CDecay B_c-sig
#
Decay MyJ/psi
  1.000        mu+       mu-        VLL;
Enddecay
#
End

