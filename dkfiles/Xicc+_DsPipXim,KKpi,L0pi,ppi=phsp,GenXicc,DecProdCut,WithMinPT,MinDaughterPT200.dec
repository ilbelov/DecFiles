# EventType: 26167152
#
# Descriptor: [Xi_cc+ -> (D_s+ -> K+ K- pi+) pi+ (Xi- -> (Lambda0 -> p+ pi-) pi-)]cc
#
# NickName: Xicc+_DsPipXim,KKpi,L0pi,ppi=phsp,GenXicc,DecProdCut,WithMinPT,MinDaughterPT200
#
# Production: GenXicc
#
# Cuts: XiccDaughtersInLHCbAndWithMinPT
#
# CutsOptions: MinXiccPT 500*MeV MinDaughterPT 200*MeV
#
# CPUTime: < 1 min
#
# Documentation: Xicc+ decay to Ds pip Xim by phase space model, Ds decays by Dalitzm, Xim decays to (L0 -> p+pi-) pi- by phase space model.
# All daughters of Xicc+ are required to be in the acceptance of LHCb and with PT>200 MeV 
# and the Xicc+ PT is required to be larger than 500 MeV.
# EndDocumentation
#
# PhysicsWG: Charm 
# Tested: Yes
# Responsible: Ziyi Wang, Miroslav Saur
# Email: ziyi.wang@cern.ch, miroslav.saur@cern.ch
# Date: 20210914
#
Alias      MyD_s-     D_s-
Alias      MyD_s+     D_s+
ChargeConj MyD_s+     MyD_s-
#
Alias      MyXim       Xi-
Alias      Myanti-Xim  anti-Xi+
ChargeConj MyXim       Myanti-Xim
#
Alias      MyL0        Lambda0
Alias      Myanti-L0   anti-Lambda0
ChargeConj MyL0       Myanti-L0
#
#
Decay Xi_cc+sig
  1.000   MyD_s+   pi+   MyXim           PHSP;
Enddecay
CDecay anti-Xi_cc-sig
#
#
Decay MyD_s+
  1.000        K+        K-        pi+             D_DALITZ;
Enddecay
CDecay MyD_s-
#
Decay MyXim
  1.000 MyL0 pi- PHSP;
Enddecay
CDecay Myanti-Xim
#
Decay MyL0
  1.000 p+ pi- PHSP;
Enddecay
CDecay Myanti-L0
#
#
End
#
