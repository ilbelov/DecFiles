# EventType: 12145405
# 
# Descriptor: [B+ -> (J/psi(1S) -> mu+ mu-) K+ (omega(782) -> pi+ pi- (pi0 -> gamma gamma))]cc
# 
# NickName: Bu_KOmegaJpsi,pi0pi+pi-=PHSP,mm=TightCut,THREEBODYPHSP
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = '^[ B+ =>  ^(J/psi(1S) => ^mu+ ^mu- )  ^K+ (omega(782) => ^pi+ ^pi- (pi0 -> ^gamma ^gamma) ) ]CC'
# tightCut.Cuts      =    {
#     'gamma'     : ' goodGamma ' ,
#     '[mu+]cc'   : ' goodMuon  ' ,
#     '[K+]cc'    : ' goodKaon  ' ,
#     '[pi+]cc'   : ' goodPion  ' ,
#     'J/psi(1S)' : ' goodPsi   ' ,
#     '[B+]cc'    : ' goodB     '}
# tightCut.Preambulo += [  "GY = LoKi.GenParticles.Rapidity()" ]
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import ns',
#     'from GaudiKernel.PhysicalConstants import c_light',
#     'inAcc     = in_range ( 0.005 , GTHETA , 0.400 ) ' ,
#     'inEcalX   = abs ( GPX / GPZ ) < 4.5 / 12.5      ' ,
#     'inEcalY   = abs ( GPY / GPZ ) < 3.5 / 12.5      ' ,
#     'inEcalHole= (abs ( GPX / GPZ ) < 0.25 / 12.5) & (abs ( GPY / GPY ) < 0.25 / 12.5)' ,
#     'goodMuon  = ( GPT > 500  * MeV ) & ( GP > 6 * GeV )     & inAcc   ' ,
#     'goodKaon  = ( GPT > 150  * MeV )                        & inAcc   ' ,
#     'goodPion  = ( GPT > 150  * MeV )                        & inAcc   ' ,
#     'goodGamma = ( 0 < GPZ ) & ( 150 * MeV < GPT ) & inEcalX & inEcalY & ~inEcalHole' ,
#     'goodPsi   = ( GPT > 1000  * MeV ) & in_range( 1.8 , GY , 4.5 )    ' ,
#     'JpsiOmegaCUT =  ( GMASS("J/psi(1S)"== GID , "omega(782)"== GID) < 4000 * MeV )',
#     'goodB     = ( GCTAU > 0.1e-3 * ns * c_light ) & JpsiOmegaCUT']
#
# EndInsertPythonCode
#
# Documentation: Decay file for B+ -> K+ (Omega -> pi+ pi- (pi0 -> gamma gamma)) (Jpsi -> mu+ mu-), PHSP. Tight cut.
# Used for Dalitz analysis for the decay X(3872) -> Omega Jpsi. With correct omega decay model and mass cut.
# EndDocumentation
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Lorenzo Capriotti
# Email: lorenzo.capriotti@cern.ch
# Date: 20200513
# CPUTime: 2 min
#
#
Alias MyJpsi J/psi
ChargeConj MyJpsi MyJpsi
Alias MyW omega
ChargeConj MyW MyW
Alias MyP0 pi0
ChargeConj MyP0 MyP0
#
Decay B+sig
  1.000    MyJpsi    MyW    K+  THREEBODYPHSP 14.00 16.40;
Enddecay
CDecay B-sig
#
Decay MyJpsi
  1.000    mu+    mu-   PHOTOS   VLL;
Enddecay
#
Decay MyW
1.000    pi+    pi-    MyP0   PHOTOS   OMEGA_DALITZ;
Enddecay
#
Decay MyP0
1.000    gamma    gamma     PHSP;
Enddecay
#
End
#
