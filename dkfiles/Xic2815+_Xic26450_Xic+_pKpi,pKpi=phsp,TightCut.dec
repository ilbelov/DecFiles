# EventType: 26165070
#
# Descriptor: [ Sigma_c*+ ->  (Xi_c0 -> (Xi_c+ -> p+ K- pi+) pi-) pi+ ]cc
#
# NickName: Xic2815+_Xic26450_Xic+_pKpi,pKpi=phsp,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# CPUTime: 1 min
#
# Documentation: (prompt) Excited Xi_c+(2815) decay according to Dalitz decay model with tight cuts.
#                 Xi_c+(2815): Mass = 2816.67 MeV and Width = 2.43 MeV
#                 Xi_c0(2645): Mass = 2645.32 MeV and Width = 2.35 MeV
# EndDocumentation
#
# ParticleValue: "Sigma_c*+  486  4214  1.0 2.81674000 2.708691e-22 Sigma_c*+       4214 0.0", "Sigma_c*~- 487 -4214 -1.0 2.81674000 2.708691e-22 anti-Sigma_c*- -4214 0.0", "Xi_c0      106  4132  0.0 2.6463     2.80085e-022 Xi_c0           4132 0.0", "Xi_c~0     107 -4132  0.0 2.6463     2.80085e-022 anti-Xi_c0     -4132 0.0"
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation() 
# signal     = generation.SignalPlain 
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut   = signal.TightCut
# tightCut.Decay     = '[ Sigma_c*+ => (Xi_c0 => ^(Xi_c+ ==> ^p+ ^K- ^pi+) ^pi-) ^pi+ ]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter,micrometer,MeV,GeV',
#     'inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )         ' ,
#     'inEta        =  in_range ( 1.95  , GETA   , 5.050 )         ' ,
#     'inY          =  in_range ( 1.95  , GY   , 5.050 )         ' ,
#     'fastTrack    =  ( GP  > 3.0 * GeV ) & (GPT > 90. * MeV)                         ' , 
#     'goodTrack    =  inAcc & inEta                               ' ,     
#     'goodXic       =  ( GPT > 0.9 * GeV )  & inY                       ' ,
#     'Bancestors   =  GNINTREE ( GBEAUTY , HepMC.ancestors )      ' ,
#     'notFromB     =  0 == Bancestors                             ' ,
# ]
# tightCut.Cuts     =    {
#     '[Xi_c+]cc'      : 'goodXic    & notFromB' ,
#     '[K-]cc'         : 'goodTrack & fastTrack' ,
#     '[pi+]cc'        : 'goodTrack & fastTrack' , 
#     '[p+]cc'         : 'goodTrack & fastTrack & ( GP > 9 * GeV ) '
#     }
# EndInsertPythonCode
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: vchuliko
# Email:       vladimir.chulikov@cern.ch
# Date: 20230712

Alias MyXi_c0       Xi_c0
Alias Myanti-Xi_c0  anti-Xi_c0
ChargeConj MyXi_c0  Myanti-Xi_c0

Alias MyXi_c+       Xi_c+
Alias Myanti-Xi_c-  anti-Xi_c-
ChargeConj MyXi_c+  Myanti-Xi_c-

Decay Sigma_c*+sig
  1.000         MyXi_c0   pi+  PHSP;
Enddecay
CDecay anti-Sigma_c*-sig

Decay MyXi_c0
  1.000         MyXi_c+   pi-  PHSP;
Enddecay
CDecay Myanti-Xi_c0

Decay MyXi_c+
  1.000         p+      K-      pi+     PHSP;
Enddecay
CDecay Myanti-Xi_c-
#
End 

