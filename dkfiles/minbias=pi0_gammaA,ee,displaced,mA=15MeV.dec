# EventType: 30122208
#
# Descriptor: [pi0 -> gamma (A' -> e+ e-)]
#
# NickName: minbias=pi0_gammaA,ee,displaced,mA=15MeV
#
# Cuts: None
# FullEventCuts: LoKi::FullGenEventCut/TightCut
# Sample: MinimumBias
#
# Documentation:
# For efficiency studies for dark-photon/true-muonium search in
# pi0 -> gamma (A' -> e+ e-)
# H_30 redefined to have suitable mass and lifetime to model displaced dark photon
# EndDocumentation
#
# InsertPythonCode:
#
# from Configurables import LoKi__FullGenEventCut
# Generation().FullGenEventCutTool = "LoKi::FullGenEventCut/TightCut"
# Generation().addTool( LoKi__FullGenEventCut, "TightCut" )
# tightCut = Generation().TightCut
# tightCut.OutputLevel=2
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import MeV",
#     "dectree = GDECTREE('pi0 => gamma (H_30 => e+ e-)')",
#     "inAcc = in_range(0.010, GTHETA, 0.400)",
#     "fidE = (GPT > 500 * MeV) & (GP > 3000 * MeV)",
#     "fidG = (GPT > 500 * MeV)",
#     "isGoodE = (('e+' == GABSID) & inAcc & fidE)",
#     "isGoodG = (('gamma' == GABSID) & inAcc & fidG)",
#     "isGoodA = ('H_30' == GABSID) & GCHILDCUT(isGoodE, 1) & GCHILDCUT(isGoodE, 2)",
#     "hasGoodA = GCHILDCUT(isGoodA, 2)",
#     "hasGoodG = GCHILDCUT(isGoodG, 1)",
#     "isGoodH = (dectree & hasGoodA & hasGoodG)",
# ]
# tightCut.Code = "(has(isGoodH))"
#
# EndInsertPythonCode
#
# ParticleValue: "H_30 89 36 0.0 0.015 1e-10 A0 36 0"
#
# PhysicsWG: Exotica
# Tested: Yes
# CPUTime: <1min
# Responsible: Michael K. Wilkinson
# Email: michael.k.wilkinson@cern.ch
# Date: 20220418

Alias       MyA   A0
ChargeConj  MyA   MyA

Decay pi0
      1.0	gamma MyA	PHSP;
Enddecay

Decay MyA
      1.0	e+    e-	PHSP;
Enddecay

End
