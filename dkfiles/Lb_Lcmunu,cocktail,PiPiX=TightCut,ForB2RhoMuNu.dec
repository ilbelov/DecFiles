# EventType: 15874005
#
# Descriptor: {[Lambda_b0 -> (Lambda_c+ => pi+ pi- X) mu- nu_mu~]cc}
#
# NickName: Lb_Lcmunu,cocktail,PiPiX=TightCut,ForB2RhoMuNu
#
# Cuts: LoKi::GenCutTool/TightCut 
# 
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# SignalFilter = gen.SignalPlain.TightCut
# SignalFilter.Decay = "^( Beauty --> (Charm --> pi+ pi- ...) [mu-]cc  ...)"
# SignalFilter.Preambulo += [
#  "from GaudiKernel.SystemOfUnits import  GeV",
#  "inAcc                = in_range ( 0.005 , GTHETA , 0.400 ) &  in_range ( 1.9 , GETA , 5.1 )", 
#  "muCuts               = (0 < GNINTREE ( ('mu-' == GABSID ) & (GP > 5 * GeV) &  (GPT > 1.2 * GeV)  & inAcc))",
#  "piPlusCuts           = (0 < GNINTREE ( ('pi+' == GID ) & (GP > 1.5 * GeV) & (GPT > 0.35 * GeV)  & inAcc ))",
#  "piMinusCuts          = (0 < GNINTREE ( ('pi-' == GID ) & (GP > 1.5 * GeV) & (GPT > 0.35 * GeV)  & inAcc ))",
#  "piMaxPT              = (GMAXTREE( GPT, ('pi+' == GABSID) & inAcc & (GP > 1.5 * GeV)) > 0.85 * GeV )",
#  "piMaxP               = (GMAXTREE( GP, ('pi+' == GABSID) & inAcc & (GPT > 0.35 * GeV)) > 4.5 * GeV )",
#  "allcuts              = ( muCuts & piPlusCuts & piMinusCuts & piMaxPT & piMaxP )"
#  ]
# SignalFilter.Cuts =  { "Beauty" : "allcuts" }
# EndInsertPythonCode
#
# Documentation: Lambda_b -> Lambda_c mu nu, Lambda_c -> pi pi X events, with cuts optimised for B -> rho mu nu analysis.
# EndDocumentation 
# 
# PhysicsWG: B2SL 
# Tested: Yes 
# CPUTime: 8min
# Responsible: Michel De Cian
# Email: michel.de.cian@cern.ch 
# Date: 20220317
#
#
Alias           MyLambda_c+			Lambda_c+
Alias           MyAntiLambda_c-			anti-Lambda_c-
ChargeConj      MyLambda_c+			MyAntiLambda_c-
#
Alias           MyLambda_c(2593)+		Lambda_c(2593)+
Alias           MyAntiLambda_c(2593)-		anti-Lambda_c(2593)-
ChargeConj      MyLambda_c(2593)+		MyAntiLambda_c(2593)-
#
Alias           MyLambda_c(2625)+		Lambda_c(2625)+
Alias           MyAntiLambda_c(2625)-		anti-Lambda_c(2625)-
ChargeConj      MyLambda_c(2625)+		MyAntiLambda_c(2625)-
#
Alias           MySigma_c*++                    Sigma_c*++
Alias           MyAntiSigma_c*--                anti-Sigma_c*--
ChargeConj      MySigma_c*++                    MyAntiSigma_c*--
#
Alias           MySigma_c*0                     Sigma_c*0
Alias           MyAntiSigma_c*0                 anti-Sigma_c*0
ChargeConj      MySigma_c*0                     MyAntiSigma_c*0
#
Alias           MySigma_c++                     Sigma_c++
Alias           MyAntiSigma_c--                 anti-Sigma_c--
ChargeConj      MySigma_c++                     MyAntiSigma_c--
#
Alias           MySigma_c0                      Sigma_c0
Alias           MyAntiSigma_c0                  anti-Sigma_c0
ChargeConj      MySigma_c0                      MyAntiSigma_c0
#
Alias 		MySigma*+             		Sigma*+
Alias 		MyAntiSigma*-        		anti-Sigma*-
ChargeConj      MySigma*+			MyAntiSigma*-
#
Alias 		MySigma*-             		Sigma*-
Alias 		MyAntiSigma*+        		anti-Sigma*+
ChargeConj      MySigma*-			MyAntiSigma*+
#
Alias 		MyLambda0             		Lambda0
Alias 		MyAntiLambda0        		anti-Lambda0
ChargeConj      MyLambda0			MyAntiLambda0
#
Alias		Myf_980_0			f_0
ChargeConj 	Myf_980_0 			Myf_980_0
#
Alias		MyEta				eta
ChargeConj 	MyEta  				MyEta
#
Alias		MyPhi				phi
ChargeConj 	MyPhi  				MyPhi
#
Alias		MyOmega				omega
ChargeConj 	MyOmega  			MyOmega
#
Alias		MyKst+				K*+
Alias      	MyKst-				K*-
ChargeConj 	MyKst+    			MyKst-
#
Alias		MyKst0				K*0
Alias      	MyAntiKst0  			anti-K*0
ChargeConj 	MyKst0  			MyAntiKst0
#
# LAMBDA_B DECAYS
#
Decay Lambda_b0sig
 0.062000 MyLambda_c+		mu-	anti-nu_mu			Lb2Baryonlnu 1 1 1 1;
 0.0079   MyLambda_c(2593)+     mu-     anti-nu_mu      		Lb2Baryonlnu 1 1 1 1;
 0.0130   MyLambda_c(2625)+     mu-     anti-nu_mu              	Lb2Baryonlnu 1 1 1 1;
 0.056    MyLambda_c+ 		pi+ 	pi- 	mu-  	anti-nu_mu     	PHOTOS PHSP;  
Enddecay
CDecay anti-Lambda_b0sig
#
# EXCITED LAMBDA_C / SIGMA_C DECAYS
# The PDG does not know the pi0 pi0 channel, so we assume it is the remaining part
#
Decay MyLambda_c(2593)+
 0.18     MyLambda_c+		pi+     pi-				PHOTOS PHSP;
 0.34     MyLambda_c+   	pi0     pi0				PHOTOS PHSP;
 0.24     MySigma_c++  		pi-     				PHOTOS PHSP;
 0.24     MySigma_c0   		pi+     				PHOTOS PHSP;
Enddecay
CDecay MyAntiLambda_c(2593)-
#
# The PDG does not know the pi0 pi0 channel, so we assume it is the remaining part
#
Decay MyLambda_c(2625)+
 0.67	  MyLambda_c+		pi+     pi-				PHOTOS PHSP;
 0.33  	  MyLambda_c+      	pi0     pi0     			PHOTOS PHSP;
Enddecay
CDecay MyAntiLambda_c(2625)-
#
Decay MySigma_c++
 1.0	  MyLambda_c+		pi+					PHOTOS PHSP;
Enddecay
CDecay MyAntiSigma_c--
#
Decay MySigma_c0
 1.0      MyLambda_c+		pi-					PHOTOS PHSP;
Enddecay
CDecay MyAntiSigma_c0
#
# LAMBDA_C DECAYS
#
Decay MyLambda_c+
 0.032000 p+			anti-K0	pi+	pi-			PHOTOS PHSP;
 0.014000 p+ 			MyKst-  pi+ 				PHOTOS PHSP;
 0.001400 p+ 			K- 	pi+ 	pi+ 	pi- 		PHOTOS PHSP;
# 
 0.001240 p+			eta					PHOTOS PHSP;
 0.000900 p+ 			omega 					PHOTOS PHSP;
 0.001100 p+ 			pi+ 	pi- 				PHOTOS PHSP;
 0.003500 p+ 			Myf_980_0				PHOTOS PHSP;
 0.00180  p+ 			pi+ 	pi- 	pi+ 	pi-      	PHOTOS PHSP;
 0.00082  p+ 			MyPhi                			PHOTOS PHSP;
#
 0.01300  MyLambda0		pi+					PHOTOS PHSP;
 0.07100  MyLambda0 		pi+ 	pi0				PHOTOS PHSP;
 0.01000  MySigma*+ 		pi+ 	pi-				PHOTOS PHSP;
 0.00760  MySigma*- 		pi+ 	pi+				PHOTOS PHSP;
 0.01500  Lambda0 		pi+ 	rho0				PHOTOS PHSP;
 0.00500  MySigma*+ 		rho0					PHOTOS PHSP;
 0.00900  Lambda0 		pi+ 	MyEta				PHOTOS PHSP;
 0.00900  MySigma*+ 		MyEta					PHOTOS PHSP;
 0.01500  Lambda0 		pi+ 	MyOmega				PHOTOS PHSP;
 0.01290  Sigma0 		pi+ 					PHOTOS PHSP;
 0.00440  Sigma+ 		eta 					PHOTOS PHSP;
 0.01500  Sigma+ 		eta' 					PHOTOS PHSP;	
 0.04500  Sigma+ 		pi+ 	pi- 				PHOTOS PHSP;	
 0.01870  Sigma- 		pi+ 	pi+ 				PHOTOS PHSP;		
 0.03500  Sigma0 		pi+ 	pi0 				PHOTOS PHSP;	
 0.01100  Sigma0 		pi- 	pi+ 	pi+ 			PHOTOS PHSP;	
 0.04500  Sigma+ 		MyOmega	  				PHOTOS PHSP;	
 0.02100  Sigma- 		pi0 	pi+ 	pi+ 			PHOTOS PHSP;
 0.04500  Sigma+ 		MyPhi	 	  			PHOTOS PHSP;	
 0.00620  Xi- 			K+ 	pi+ 	  			PHOTOS PHSP;		
#
 0.00350  Sigma+		MyKst0					PHOTOS PHSP;
Enddecay
CDecay MyAntiLambda_c-
#
# LIGHT RESONANCES DECAYS
#
Decay MySigma*+
 1.00     Lambda0		pi+					PHOTOS PHSP;
Enddecay
CDecay MyAntiSigma*-
#
Decay MySigma*-
 1.00     Lambda0		pi-					PHOTOS PHSP;
Enddecay
CDecay MyAntiSigma*+
#
Decay MyLambda0
 1.00     p+			pi-					PHOTOS HELAMP 0.936 0.0 0.351 0.0;
Enddecay
CDecay MyAntiLambda0
#
Decay MyEta
 1.00     pi-			pi+      pi0				PHOTOS ETA_DALITZ;
Enddecay
#
Decay MyOmega
 1.0      pi-			pi+      pi0				PHOTOS OMEGA_DALITZ;
Enddecay
#
Decay MyPhi   
 0.3048   pi+			pi-	 pi0				PHI_DALITZ; 
Enddecay
#
Decay Myf_980_0
 1.00	  pi+			pi-					PHOTOS PHSP;
Enddecay
#
Decay MyKst-   
  1.00     K-			pi0					PHOTOS VSS; 
Enddecay
CDecay MyKst+
#
Decay MyKst0   
 1.00      K+			pi-					PHOTOS VSS; 
Enddecay
CDecay MyAntiKst0
#
End
