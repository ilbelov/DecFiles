# EventType: 14877500
#
# Descriptor: [B_c+ -> K+ (D*0 -> (D0 -> (K_S0 -> pi+ pi-) pi+ pi- pi0) pi0) (J/psi -> mu+ mu-)]cc
#
# NickName: Bc_JpsiD0,mm,D0+=cocktail,pipipi=DecProdCut,CntCut
#
# Production: BcVegPy
#
# Cuts: BcChargedNumInLHCb
# CutsOptions: MuNumInLHCb 2 HadNumInLHCb 3 HadPt 250. MuPt 450.
#
# Documentation: Sum of Bc -> Jpsi D0 & D*0 modes. Jpsi -> mu+ mu-, D0/D*0 -> pi+ pi- pi X
#
# EndDocumentation
#
# PhysicsWG: B2SL
# CPUTime:<1 min
# Tested: Yes
# Responsible: Wenqian Huang
# Email: wenqian.huang@cern.ch
# Date: 20170421
#
Alias    MyJpsi    J/psi
ChargeConj    MyJpsi    MyJpsi
#
Alias    Myf_1    f_1
ChargeConj    Myf_1    Myf_1
#
Alias    MyD*+    D*+
Alias    MyD*-    D*-
ChargeConj    MyD*+    MyD*-
#
Alias    MyD+    D+
Alias    MyD-    D-
ChargeConj    MyD+    MyD-
#
Alias    MyD0    D0
Alias    Myanti-D0    anti-D0
ChargeConj    MyD0    Myanti-D0
#
Alias    MyD*0    D*0
Alias    Myanti-D*0    anti-D*0
ChargeConj    MyD*0    Myanti-D*0
#
Alias    Myphi    phi
ChargeConj    Myphi    Myphi
#
Alias    MyK_1+    K_1+
Alias    MyK_1-    K_1-
ChargeConj    MyK_1+    MyK_1-
#
Alias    MyK_10    K_10
Alias    Myanti-K_10    anti-K_10
ChargeConj    MyK_10    Myanti-K_10
#
Alias    MyK_2*-    K_2*-
Alias    MyK_2*+    K_2*+
ChargeConj    MyK_2*-    MyK_2*+
#
Alias    MyK*+    K*+
Alias    MyK*-    K*-
ChargeConj    MyK*+    MyK*-
#
Alias    MyK*0    K*0
Alias    Myanti-K*0    anti-K*0
ChargeConj    MyK*0    Myanti-K*0
#
Alias    Myeta'    eta'
ChargeConj    Myeta'    Myeta'
#
Alias    MyK0    K0
Alias    Myanti-K0    anti-K0
ChargeConj    MyK0    Myanti-K0
#
Alias    Myomega    omega
ChargeConj    Myomega    Myomega
#
Alias    Mya_00    a_00
ChargeConj    Mya_00    Mya_00
#
Alias    Mya_0+    a_0+
Alias    Mya_0-    a_0-
ChargeConj    Mya_0+    Mya_0-
#
Alias    MyK_S0    K_S0
ChargeConj    MyK_S0    MyK_S0
#
Alias    Myeta    eta
ChargeConj    Myeta    Myeta
#
Alias    Mya_1+1prong    a_1+
Alias    Mya_1-1prong    a_1-
ChargeConj    Mya_1+1prong    Mya_1-1prong
#
Alias    Mya_1+3prong    a_1+
Alias    Mya_1-3prong    a_1-
ChargeConj    Mya_1+3prong    Mya_1-3prong
#
Alias    Myf_0    f_0
ChargeConj    Myf_0    Myf_0
#
Alias    Myrho0    rho0
ChargeConj    Myrho0    Myrho0
#
Alias    Myrho+    rho+
Alias    Myrho-    rho-
ChargeConj    Myrho+    Myrho-
#
Decay    B_c+sig
0.001445770     MyJpsi    MyD0    K+                    PHSP;
0.001473598     MyJpsi    MyD0    MyK*+                    PHSP;
0.005503255     MyJpsi    MyD*0    K+                    PHSP;
0.002947196     MyJpsi    MyD*0    MyK*+                    PHSP;
Enddecay
CDecay    B_c-sig
#
Decay    MyJpsi
1.000000000     mu+    mu-                        PHOTOS    VLL;
Enddecay
#
Decay    MyD*0
0.288687676     MyD0    pi0                        VSS;
0.177689830     MyD0    gamma                        VSP_PWAVE;
Enddecay
CDecay    Myanti-D*0
#
Decay    MyD0
0.005004360     MyK*-    e+    nu_e                    PHOTOS    ISGW2;
0.000237258     MyK_1-    e+    nu_e                    PHOTOS    ISGW2;
0.000398700     MyK_2*-    e+    nu_e                    PHOTOS    ISGW2;
0.002890000     pi-    e+    nu_e                    PHOTOS    ISGW2;
0.001900000     Myrho-    e+    nu_e                    PHOTOS    ISGW2;
0.009353498     Myanti-K0    pi-    e+    nu_e                PHOTOS    PHSP;
0.004566190     MyK*-    mu+    nu_mu                    PHOTOS    ISGW2;
0.000254596     MyK_1-    mu+    nu_mu                    PHOTOS    ISGW2;
0.000398700     MyK_2*-    mu+    nu_mu                    PHOTOS    ISGW2;
0.002015940     Myrho-    mu+    nu_mu                    PHOTOS    ISGW2;
0.000349187     Myanti-K0    pi-    mu+    nu_mu                PHOTOS    PHSP;
0.038900000     K-    pi+                        PHSP;
0.008452790     MyK_S0    pi0                        PHSP;
0.003332866     MyK_S0    Myeta                        PHSP;
0.001040499     K_L0    Myeta                        PHSP;
0.007877747     MyK_S0    Myeta'                        PHSP;
0.004508868     K_L0    Myeta'                        PHSP;
0.010791201     Myomega    MyK_S0                        SVS;
0.009916743     Myomega    K_L0                        SVS;
0.000573663     Myanti-K*0    Myeta                        SVS;
0.000512927     Myanti-K*0    Myeta'                        SVS;
0.038376000     Mya_1+3prong    K-                        SVS;
0.015595525     MyK*-    Myrho+                        SVV_HELAMP    1    0    1    0    1    0.0;
0.001829800     Myanti-K*0    Myrho0                        SVV_HELAMP    1    0    1    0    1    0.0;
0.010119068     Myanti-K*0    Myomega                        SVV_HELAMP    1    0    1    0    1    0.0;
0.004994896     MyK_1-    pi+                        SVS;
0.002088387     Myanti-K_10    pi0                        SVS;
0.029400000     MyK_S0    pi+    pi-                    D_DALITZ;
0.027856619     K_L0    pi+    pi-                    D_DALITZ;
0.005750669     MyK_S0    pi0    pi0                    PHSP;
0.024000000     Myanti-K*0    pi+    pi-                    PHSP;
0.001231004     Myanti-K*0    pi0    pi0                    PHSP;
0.002113215     MyK*-    pi+    pi0                    PHSP;
0.001703065     K-    pi+    Myrho0                    PHSP;
0.017279091     K-    pi+    Myomega                    PHSP;
0.002507226     K-    pi+    Myeta                    PHSP;
0.003765719     K-    pi+    Myeta'                    PHSP;
0.013300000     K-    pi+    pi+    pi-                PHSP;
0.054000000     MyK_S0    pi+    pi-    pi0                PHSP;
0.010079698     K_L0    pi+    pi-    pi0                PHSP;
0.002800000     MyK_S0    pi+    pi-    pi+    pi-            PHSP;
0.002684865     K_L0    pi+    pi-    pi+    pi-            PHSP;
0.001655309     Myphi    MyK_S0                        SVS;
0.000800476     Myphi    K_L0                        SVS;
0.003221760     MyK_S0    K+    K-                    PHSP;
0.000922472     MyK_S0    MyK_S0    MyK_S0                    PHSP;
0.000172075     MyK_S0    MyK_S0                        PHSP;
0.000154720     MyK*0    Myanti-K0                        SVS;
0.000066748     Myanti-K*0    MyK_S0                        SVS;
0.000010612     Myanti-K*0    K_L0                        SVS;
0.000112000     MyK*-    K+                        SVS;
0.000314869     MyK*+    K-                        SVS;
0.000279933     Myanti-K*0    MyK*0                        SVV_HELAMP    1    0    1    0    1    0.0;
0.000281248     Myphi    pi0                        SVS;
0.001007970     Myphi    pi+    pi-                    PHSP;
0.002430000     K+    K-    pi+    pi-                PHSP;
0.001159245     MyK_S0    MyK_S0    pi+    pi-                PHSP;
0.001255381     K_L0    K_L0    pi+    pi-                PHSP;
0.000787372     Myanti-K0    MyK0    pi0    pi0                PHSP;
0.001397000     pi+    pi-                        PHSP;
0.000175113     Myeta    pi0                        PHSP;
0.000406698     Myeta'    pi0                        PHSP;
0.000788847     Myeta    Myeta                        PHSP;
0.009800000     Myrho+    pi-                        SVS;
0.004970000     Myrho-    pi+                        SVS;
0.003730000     Myrho0    pi0                        SVS;
0.001209564     pi+    pi-    pi0                    PHSP;
0.005620000     pi+    pi+    pi-    pi-                PHSP;
0.009360000     pi+    pi-    pi0    pi0                PHSP;
0.001510000     pi+    pi-    pi+    pi-    pi0            PHSP;
0.005498017     pi+    pi-    pi0    pi0    pi0            PHSP;
0.000420000     pi+    pi-    pi+    pi-    pi+    pi-        PHSP;
0.000029585     pi-    MyK*+                        PHSP;
0.000247411     K+    pi-    pi+    pi-                PHSP;
0.000078322     Myphi    Myeta                        PHSP;
0.002200393     Myanti-K*0    pi+    pi-    pi0                PHSP;
0.000220000     K-    pi+    pi-    pi+    pi-    pi+        PHSP;
0.003100000     K+    K-    pi+    pi-    pi0            PHSP;
0.000221000     K+    K-    K-    pi+                PHSP;
0.004350594     MyK_S0    Myeta    pi0                    PHSP;
0.001002890     K_L0    Myeta    pi0                    PHSP;
0.001801414     MyK_S0    K+    pi-                    PHSP;
0.002424981     MyK_S0    K-    pi+                    PHSP;
0.000310000     MyK_S0    MyK_S0    K+    pi-                PHSP;
0.000310000     MyK_S0    MyK_S0    K-    pi+                PHSP;
0.001820000     Myrho0    Myrho0                        PHSP;
0.001090000     Myeta    pi+    pi-                    PHSP;
0.001600000     Myomega    pi+    pi-                    PHSP;
0.000450000     Myeta'    pi+    pi-                    PHSP;
0.000804295     Myeta    Myeta'                        PHSP;
0.000010624     Myphi    gamma                        PHSP;
0.000037986     Myanti-K*0    gamma                        PHSP;
Enddecay
CDecay    Myanti-D0
#
Decay    Myf_1
0.110000000     Myrho0    pi+    pi-                    PHSP;
0.043364159     Myrho0    pi0    pi0                    PHSP;
0.043442860     Myrho+    pi-    pi0                    PHSP;
0.043442860     Myrho-    pi+    pi0                    PHSP;
0.024389753     Mya_00    pi0                        VSS;
0.026528039     Mya_0+    pi-                        VSS;
0.026528039     Mya_0-    pi+                        VSS;
0.023687032     Myeta    pi+    pi-                    PHSP;
0.011843516     Myeta    pi0    pi0                    PHSP;
0.005815713     MyK0    Myanti-K0    pi0                    PHSP;
0.007006848     Myanti-K0    K+    pi-                    PHSP;
0.007006848     MyK0    K-    pi+                    PHSP;
0.055000000     gamma    Myrho0                        PHSP;
0.220000000     pi0    pi0    pi+    pi-                PHSP;
0.000291187     Myphi    gamma                        PHSP;
Enddecay
#
Decay    Myphi
0.236955271     K_L0    MyK_S0                        VSS;
0.042500000     Myrho+    pi-                        VVS_PWAVE    1    0    0    0    0    0.0;
0.042500000     Myrho0    pi0                        VVS_PWAVE    1    0    0    0    0    0.0;
0.042500000     Myrho-    pi+                        VVS_PWAVE    1    0    0    0    0    0.0;
0.025000000     pi+    pi-    pi0                    PHSP;
0.003581610     Myeta    gamma                        VSP_PWAVE;
0.000031466     Myeta    e+    e-                    PHSP;
0.000214677     Myf_0    gamma                        PHSP;
0.000074000     pi+    pi-                        PHSP;
0.000042743     Myomega    pi0                        PHSP;
0.000041000     pi+    pi-    gamma                    PHSP;
0.000004000     pi+    pi-    pi+    pi-                PHSP;
0.000019892     pi0    Myeta    gamma                    PHSP;
0.000031381     Myeta'    gamma                        PHSP;
Enddecay
#
Decay    MyK_1+
0.012356944     MyK*0    pi+                        VVS_PWAVE    1    0    0    0    0    0.0;
0.012291816     MyK*+    pi0                        VVS_PWAVE    1    0    0    0    0    0.0;
0.100036845     Myomega    K+                        VVS_PWAVE    1    0    0    0    0    0.0;
0.144400000     K+    pi+    pi-                    PHSP;
0.043095374     MyK0    pi+    pi0                    PHSP;
Enddecay
CDecay    MyK_1-
#
Decay    MyK_10
0.048499617     Myrho0    MyK0                        VVS_PWAVE    1    0    0    0    0    0.0;
0.024606693     MyK*+    pi-                        VVS_PWAVE    1    0    0    0    0    0.0;
0.103488339     Myomega    MyK0                        VVS_PWAVE    1    0    0    0    0    0.0;
0.144400000     MyK0    pi+    pi-                    PHSP;
Enddecay
CDecay    Myanti-K_10
#
Decay    MyK_2*-
0.115706229     Myanti-K0    pi-                        TSS;
0.019050771     Myanti-K*0    pi-                        TVS_PWAVE    0    0    1    0    0    0.0;
0.019256409     MyK*-    pi0                        TVS_PWAVE    0    0    1    0    0    0.0;
0.005211457     Myanti-K*0    pi-    pi0                    PHSP;
0.045000000     MyK*-    pi+    pi-                    PHSP;
0.010377706     MyK*-    pi0    pi0                    PHSP;
0.020092698     Myrho-    MyK0                        TVS_PWAVE    0    0    1    0    0    0.0;
0.029000000     Myrho0    K-                        TVS_PWAVE    0    0    1    0    0    0.0;
0.026373350     Myomega    K-                        TVS_PWAVE    0    0    1    0    0    0.0;
Enddecay
CDecay    MyK_2*+
#
Decay    MyK*0
0.115117305     MyK0    pi0                        VSS;
0.000692852     MyK0    gamma                        VSP_PWAVE;
Enddecay
CDecay    Myanti-K*0
#
Decay    MyK*+
0.230615678     MyK0    pi+                        VSS;
Enddecay
CDecay    MyK*-
#
Decay    Myeta'
0.118201334     pi+    pi-    Myeta                    PHSP;
0.059374281     pi0    pi0    Myeta                    PHSP;
0.293511000     Myrho0    gamma                        SVP_HELAMP    1    0    1    0.0;
0.025009211     Myomega    gamma                        SVP_HELAMP    1    0    1    0.0;
0.003600000     pi+    pi-    pi0                    PHSP;
0.002400000     pi+    pi-    e+    e-                PHSP;
Enddecay
#
Decay    Mya_00
0.246252780     Myeta    pi0                        PHSP;
0.012001086     MyK_S0    MyK_S0                        PHSP;
Enddecay
#
Decay    Mya_0+
0.246252780     Myeta    pi+                        PHSP;
0.034642583     Myanti-K0    K+                        PHSP;
Enddecay
CDecay    Mya_0-
#
Decay    MyK0
0.346425835     MyK_S0                            PHSP;
Enddecay
#
Decay   Myanti-K0
0.346425835     MyK_S0                            PHSP;
Enddecay
#
Decay    Myomega
0.892000000     pi-    pi+    pi0                    OMEGA_DALITZ;
0.015300000     pi-    pi+                        VSS;
0.000125863     Myeta    gamma                        VSP_PWAVE;
0.001500000     pi+    pi-    gamma                    PHSP;
0.000500000     pi+    pi-    pi+    pi-                PHSP;
Enddecay
#
Decay    MyK_S0
0.691086452     pi+    pi-                        PHSP;
0.000000201     pi+    pi-    pi0                    PHSP;
0.001722185     pi+    pi-    gamma                    PHSP;
0.000042831     pi+    pi-    e+    e-                PHSP;
Enddecay
#
Decay    Myeta
0.227400000     pi-    pi+    pi0                    ETA_DALITZ;
0.046000000     gamma    pi-    pi+                    PHSP;
0.000214200     pi+    pi-    e+    e-                PHSP;
Enddecay
#
Decay    Mya_1+1prong
0.492000000     Myrho0    pi+                        VVS_PWAVE    1    0    0    0    0    0.0;
0.508000000     Myrho+    pi0                        VVS_PWAVE    1    0    0    0    0    0.0;
Enddecay
CDecay    Mya_1-1prong
#
Decay    Mya_1+3prong
0.492000000     Myrho0    pi+                        VVS_PWAVE    1    0    0    0    0    0.0;
Enddecay
CDecay    Mya_1-3prong
#
Decay    Myf_0
0.666700000     pi+    pi-                        PHSP;
Enddecay
#
Decay    Myrho0
1.000000000     pi+    pi-                        VSS;
Enddecay
#
Decay    Myrho+
1.000000000     pi+    pi0                        VSS;
Enddecay
CDecay    Myrho-
#
End
