# EventType: 11266081
#
# Descriptor: {[[B0]nos -> (D- => K+ pi- pi-) pi+ pi- pi+]cc, [[B0]os -> (D+ => K- pi+ pi+) pi- pi+ pi-]cc}
#
# NickName: Bd_D-pipipi,Kpipi=withRes,DecProdCut
#
# Cuts: DaughtersInLHCb
#
# Documentation: Includes resonances in B0 and D- decays
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: D. Craik
# Email: dcraik@cern.ch
# Date: 20240107
# CPUTime: 2 mins
#
#
Alias      Mya1+          a_1+
Alias      Mya1-          a_1-
ChargeConj Mya1+          Mya1-
#
LSNONRELBW Mya1+
BlattWeisskopf Mya1+ 0.0
Particle Mya1+ 1.17 0.50
ChangeMassMin Mya1+ 0.772
ChangeMassMax Mya1+ 3.41
#
LSNONRELBW Mya1-
BlattWeisskopf Mya1- 0.0
Particle Mya1- 1.17 0.50
ChangeMassMin Mya1- 0.772
ChangeMassMax Mya1- 3.41
#
Alias      Mysigma        sigma_0
ChargeConj Mysigma        Mysigma
#
Alias      Myrho          rho0
ChargeConj Myrho          Myrho
#
Alias      Myf2           f_2
ChargeConj Myf2           Myf2   
#
Alias      My2460         D_2*0
Alias      My2460b        anti-D_2*0
ChargeConj My2460         My2460b
#
Alias      My2420+        D_1+
Alias      My2420-        D_1-
ChargeConj My2420+        My2420-
#
Alias      MyD+           D+
Alias      MyD-           D-
ChargeConj MyD+           MyD-

Decay B0sig
#D (3pi)**
0.90  Mya1+   MyD-            SVS;
0.015 MyD-    pi+   Mysigma   PHSP;
0.03  MyD-    pi+   Myrho     PHSP;
0.015 MyD-    pi+   Myf2      PHSP;
#D** pi
0.015 My2420- pi+             SVS;
#D** (pipi)**
0.004 My2460b Mysigma         PHSP;
0.01  My2460b Myrho           PHSP;
0.011 My2460b Myf2            PHSP;
Enddecay
CDecay anti-B0sig

Decay MyD-
1.0  K+  pi-  pi-             D_DALITZ;
Enddecay
CDecay MyD+

#RESONANCES
Decay My2420-
1.0 MyD-  pi+  pi-              PHSP;
Enddecay
CDecay My2420+

Decay Mya1+
0.07  Mysigma  pi+            PHSP;
0.90  Myrho pi+               VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
0.03  Myf2  pi+               PHSP;
Enddecay
CDecay Mya1-

Decay My2460b
1.0  MyD-  pi+                TSS;
Enddecay
CDecay My2460

Decay Mysigma
1.0  pi+  pi-                 PHSP;
Enddecay

Decay Myf2
1.0  pi+  pi-                 TSS;
Enddecay

Decay Myrho
1.0  pi+  pi-                 VSS;
Enddecay

End
