# EventType: 11576020
#
# Descriptor:  [B0 -> (anti-D0 -> (a_1- -> pi- (rho0 -> pi+ pi-)) K+) pi- mu+ nu_mu]cc
#
#
# NickName: Bd_Dpimunu,a1,Kpipipi=DecProdCut,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
#
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[ B0 => ^(D~0 => ^(a_1(1260)- => ^pi-  ^(rho(770)0 => ^pi+ ^pi-)) ^K+) ^pi- ^mu+ Nu]CC'
# tightCut.Cuts      =    {
#     '[pi+]cc'   : ' goodPion  ' ,
#     '[K+]cc'    : ' goodKaon  ' ,
#     '[mu+]cc'   : ' goodMuon  ' }
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import MeV',
#     'inAcc = in_range( 0.005, GTHETA, 0.400)',
#     'goodMuon  = ( GPT > 950  * MeV ) & inAcc' ,
#     'goodKaon  = ( GPT > 220  * MeV ) & inAcc' ,
#     'goodPion  = ( GPT > 220  * MeV ) & inAcc' ]
#
# EndInsertPythonCode
#
#
# Documentation: Bd decay to D~0 pi mu nu with D~0 to a1- K+ and a1- -> rho(770)0 (-> pi pi) pi)
# All final-state products in the acceptance. 
# Tight generator level cuts applied for all final state particles 
# EndDocumentation
#
# PhysicsWG: RD
# CPUTime: < 1 min
# Tested: Yes
# Responsible: Marie Bachmayer
# Email: marie.bachmayer@cern.ch
# Date: 20190429
#

#
Alias	      MyD0	 D0
Alias	      MyantiD0	 anti-D0
ChargeConj    MyD0	 MyantiD0
Alias	      Mya_1+	 a_1+
Alias	      Mya_1-	 a_1-
ChargeConj    Mya_1+	 Mya_1-
Alias	      Myrho0	 rho0
#
Decay B0sig
  1.000       MyantiD0	pi-	mu+	nu_mu	PHOTOS    GOITY_ROBERTS;
Enddecay
CDecay anti-B0sig
#
Decay MyantiD0
  1.000		Mya_1-	K+	SVS;
Enddecay
CDecay MyD0
#
Decay Mya_1+
  1.000		Myrho0	pi+	VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Mya_1-
#
Decay Myrho0
  1.000		pi+	pi-	VSS;
Enddecay
#
End
