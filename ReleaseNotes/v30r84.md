DecFiles v30r84 2023-02-08 
==========================  
 
! 2023-02-03 - Seophine Stanislaus (MR !1278)  
   Add 2 new decay files  
   + 12165587 : Bu_Dst0K,D0pi0,KSpipi=TightCut,LooserCuts,PartRecoCuts  
   + 12165589 : Bu_Dst0pi,D0pi0,KSpipi=TightCut,LooserCuts,PartRecoCuts  
  
! 2023-02-01 - Yuya Shimizu (MR !1277)  
   Add 20 new decay files  
   + 11166097 : Bd_D0KK,KKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166095 : Bd_D0Kpi,KKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166096 : Bd_D0pipi,KKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166297 : Bd_Dst0KK,D0gamma,KKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166497 : Bd_Dst0KK,D0pi0,KKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166295 : Bd_Dst0Kpi,D0gamma,KKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166495 : Bd_Dst0Kpi,D0pi0,KKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166296 : Bd_Dst0pipi,D0gamma,KKpipi=BsqDalitz,DAmpGen,TightCut  
   + 11166496 : Bd_Dst0pipi,D0pi0,KKpipi=BsqDalitz,DAmpGen,TightCut  
   + 13166097 : Bs_D0KK,KKpipi=BssqDalitz,DAmpGen,TightCut  
   + 13166297 : Bs_Dst0KK,D0gamma,KKpipi=BssqDalitz,DAmpGen,TightCut  
   + 13166497 : Bs_Dst0KK,D0pi0,KKpipi=BssqDalitz,DAmpGen,TightCut  
   + 13166295 : Bs_Dst0Kpi,D0gamma,KKpipi=BssqDalitz,DAmpGen,TightCut  
   + 13166495 : Bs_Dst0Kpi,D0pi0,KKpipi=BssqDalitz,DAmpGen,TightCut  
   + 15166097 : Lb_D0pK,KKpipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166096 : Lb_D0ppi,KKpipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166297 : Lb_Dst0pK,D0gamma,KKpipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166497 : Lb_Dst0pK,D0pi0,KKpipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166296 : Lb_Dst0ppi,D0gamma,KKpipi=LbsqDalitz,DAmpGen,TightCut  
   + 15166496 : Lb_Dst0ppi,D0pi0,KKpipi=LbsqDalitz,DAmpGen,TightCut  
  
! 2023-01-30 - Mary Richardson-Slipper (MR !1276)  
   Add 2 new decay files  
   + 13104017 : Bs_phiphi=LHCbAmp,DecProdCut  
   + 13104018 : Bs_phiphi=LHCbAmp,dG=0,DecProdCut  
  
! 2023-01-30 - Yuya Shimizu (MR !1275)  
   Add 2 new decay files  
   + 11166192 : Bd_D0Kpi,KSKK=BsqDalitz,DPHSP,TightCut  
   + 11166191 : Bd_D0Kpi,KSpipi=BsqDalitz,DPHSP,TightCut  
  
! 2023-01-20 - Lakshan Ram Madhan Mohan (MR !1273)  
   Add new decay file  
   + 40512000 : exclu_tautau,mm=two_photon_starlight_evtGen  
  
! 2023-01-20 - Zeqing Mu (MR !1272)  
   Add new decay file  
   + 11166006 : Bd_Sigmacppi-=phsp,DecProdCut  
  
! 2022-12-15 - Antonio Romero Vidal (MR !1256)  
   Add 7 new decay files  
   + 11198061 : Bd_Dst-Ds+,D0pi,Kpipipi,pipipi=TightCut  
   + 11268001 : Bd_Dst-pipipi,D0pi,K3pi=TightCut  
   + 11898201 : Bd_DstXcIncl,K3pi,3piIncl=TightCut  
   + 11565001 : Bd_Dsttaunu,D0pi,K3pi,3pinu,tauolababar=TightCut  
   + 11565011 : Bd_Dsttaunu,D0pi,K3pi,3pipi0nu,tauola=TightCut  
   + 13898201 : Bs_DstXcIncl,K3pi,3piIncl=TightCut  
   + 12899411 : Bu_DstXcIncl,K3pi,3piIncl=TightCut  
  
