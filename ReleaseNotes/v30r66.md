DecFiles v30r66 2021-10-25 
==========================  
 
! 2021-10-22 - Vitalii Lisovskyi (MR !883)  
   Add 2 new decay files  
   + 16144541 : Xib0_JpsiXi0,mm,Lambdapi0=phsp,TightCut  
   + 16114541 : Xib0_Xi0mumu,Lambdapi=phsp,TightCut  
  
! 2021-10-21 - Niladri Sahoo (MR !881)  
   Add 21 new decay files  
   + 15154700 : Lb_JpsiLambdast1405,ee,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15154701 : Lb_JpsiLambdast1520,ee,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15154702 : Lb_JpsiLambdast1600,ee,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15154300 : Lb_JpsiSigma0,ee,Lambdagamma,ppi=phsp,DecProdCut  
   + 15124700 : Lb_Lambdast1405ee,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15114700 : Lb_Lambdast1405mumu,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15124701 : Lb_Lambdast1520ee,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15114701 : Lb_Lambdast1520mumu,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15124702 : Lb_Lambdast1600ee,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15114702 : Lb_Lambdast1600mumu,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15124300 : Lb_Sigma0ee,Lambdagamma,ppi=phsp,DecProdCut  
   + 15114300 : Lb_Sigma0mumu,Lambdagamma,ppi=phsp,DecProdCut  
   + 15156100 : Lb_psi2SLambda,Jpsipipi,ee=DecProdCut  
   + 15154703 : Lb_psi2SLambdast1405,ee,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15144710 : Lb_psi2SLambdast1405,mm,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15154704 : Lb_psi2SLambdast1520,ee,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15144711 : Lb_psi2SLambdast1520,mm,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15154705 : Lb_psi2SLambdast1600,ee,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15144712 : Lb_psi2SLambdast1600,mm,Sigmapi,Lambdagamma=phsp,DecProdCut  
   + 15154301 : Lb_psi2SSigma0,ee,Lambdagamma,ppi=phsp,DecProdCut  
   + 15144320 : Lb_psi2SSigma0,mm,Lambdagamma,ppi=phsp,DecProdCut  
  
! 2021-10-21 - Liupan An (MR !880)  
   Add new decay file  
   + 17144254 : Bsprime1_Bsstgamma,Jpsiphi,mm=TightCut  
  
! 2021-10-13 - Pablo Baladron Rodriguez (MR !857)  
   Add 8 new decay files  
   + 11166144 : Bd_Dst-pi,KSpipi=PartRecCut  
   + 11106101 : Bd_Kspipipipi=PHSP,PartRecCut  
   + 13166172 : Bs_Dspi,Kspipipi=PartRecCut  
   + 13106101 : Bs_Kspipipipi=PHSP,PartRecCut  
   + 12165529 : Bu_D0pi,KSpipipi0=PHSP,PartRecCut  
   + 12105315 : Bu_EtapKst+,rhogamma,Kspi=PartRecCut  
   + 12105314 : Bu_Kspipipigamma=PHSP,PartRecCut  
   + 12105501 : Bu_Kspipipipi0=PHSP,PartRecCut  
  
! 2021-10-13 - Miguel Ramos Pernas (MR !856)  
   Add new decay file  
   + 34122101 : Ks_ee=TightCut  
  
! 2021-10-07 - Ziyi Wang (MR !848)  
   Add 4 new decay files  
   + 26104982 : Omegac0_OmegamKp,L0K,ppi=phsp,DecProdCut,tau=274fs  
   + 26104985 : Omegac0_OmegamPip,L0K,ppi=phsp,DecProdCut,tau=274fs  
   + 26104984 : Omegac0_XimKp,L0pi,ppi=phsp,DecProdCut,tau=274fs  
   + 26104983 : Omegac0_XimPip,L0pi,ppi=phsp,DecProdCut,tau=274fs  
  
! 2021-10-06 - Zehua Xu (MR !847)  
   Add 4 new decay files  
   + 11166661 : Bd_DstKst+,D0pi,K3pi,Kpi0=TightCut  
   + 12265664 : Bu_D0Rho-,K3pi,pipi0=TightCut  
   + 12265691 : Bu_Dst0Kst+,D0gamma,K3pi,Kpi0=TightCut  
   + 12265692 : Bu_Dst0Kst+,D0pi0,K3pi,Kpi0=TightCut  
  
