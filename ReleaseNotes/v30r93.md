DecFiles v30r93 2023-07-13 
==========================  
 
! 2023-07-13 - Lais Soares Lavra (MR !1548)  
   Add new decay file  
   + 13514091 : Bs_KKtaumu,pi=DecProdCut  
  
! 2023-07-13 - Michal Kreps (MR !1547)  
   Modify 10 decay files  
   + 13166151 : Bs_Dst-KSpi+,D0pi=DecProdCut  
   + 12165107 : Bu_D0K,KSKK=gamma,DecProdCut  
   + 12165106 : Bu_D0K,KSpipi=gamma,DecProdCut  
   + 12513012 : Bu_phimunu=TightCut,BToVlnuBall  
   + 27265105 : Dst_D0pi,KSKK=mix,DecProdCut  
   + 27265104 : Dst_D0pi,KSKK=nomix,DecProdCut  
   + 27265103 : Dst_D0pi,KSpipi=mix_rbw,DecProdCut  
   + 27265102 : Dst_D0pi,KSpipi=nomix_rbw,DecProdCut  
   + 15436000 : Lb_JpsipK,hhhh=DecProdCut  
   + 18112002 : incl_Upsilons,mm=NoCut  
  
! 2023-07-13 - Arnau Brossa Gonzalo (MR !1546)  
   Add 4 new decay files  
   + 23103431 : Ds+_etapi,pipipi0,gg=TightCut  
   + 23103472 : Ds+_omegapi,pipipi0,gg=TightCut  
   + 23103441 : Ds+_phipi,pipipi0,gg=TightCut  
   + 23103491 : Ds+_pipipipi0,gg=TightCut  
  
! 2023-07-13 - Lais Soares Lavra (MR !1545)  
   Add 3 new decay files  
   + 13526000 : Bs_KKtaue,3pi=DecProdCut,tauolababar,phsp  
   + 13524000 : Bs_KKtaue,pi=DecProdCut  
   + 13516010 : Bs_KKtaumu,3pi=DecProdCut,tauolababar,phsp  
  
! 2023-07-12 - Fabian Christoph Glaser (MR !1544)  
   Add 3 new decay files  
   + 14177030 : Bc_JpsiDst,D0pi,mmK3pi=BcVegPy,DecProdCut  
   + 14175060 : Bc_JpsiDst,D0pi,mmKpi=BcVegPy,DecProdCut  
   + 14175410 : Bc_JpsiDst,D0pi,mmKpipi=BcVegPy,DecProdCut  
  
! 2023-07-12 - Lakshan Ram Madhan Mohan (MR !1543)  
   Add 5 new decay files  
   + 11524061 : Bd_Kpitaue,e=DecProdCut  
   + 11514071 : Bd_Kpitaue,mu=DecProdCut  
   + 11724442 : Bd_Kpitaue,pi=TracksInAcc  
   + 11514061 : Bd_Kpitaumu,e=DecProdCut  
   + 11714442 : Bd_Kpitaumu,pi=TracksInAcc  
  
! 2023-07-12 - Vladimir Chulikov (MR !1542)  
   Add new decay file  
   + 26165070 : Xic2815+_Xic26450_Xic+_pKpi,pKpi=phsp,TightCut  
  
! 2023-07-12 - Jinjia Zhao (MR !1541)  
   Add 2 new decay files  
   + 15146180 : Lb_JpsiLambda1520,mm,Lambdapipi,ppi=DecProdCut  
   + 15246173 : Lb_JpsiSigma1385pi,mm,Lambdapi,ppi=phsp,DecProdCut  
  
! 2023-07-11 - Dan Thompson (MR !1540)  
   Add 12 new decay files  
   + 15584084 : Lb_Lc2860enu,D0p,Kenu=LHCbAcceptance,HighVisMass,EvtGenCut  
   + 15574097 : Lb_Lc2860enu,D0p,Kmunu=LHCbAcceptance,HighVisMass,EvtGenCut  
   + 15574087 : Lb_Lc2860munu,D0p,Kenu=LHCbAcceptance,HighVisMass,EvtGenCut  
   + 15574084 : Lb_Lc2860munu,D0p,Kmunu=LHCbAcceptance,HighVisMass,EvtGenCut  
   + 15584085 : Lb_Lc2880enu,D0p,Kenu=LHCbAcceptance,HighVisMass,EvtGenCut  
   + 15574098 : Lb_Lc2880enu,D0p,Kmunu=LHCbAcceptance,HighVisMass,EvtGenCut  
   + 15574088 : Lb_Lc2880munu,D0p,Kenu=LHCbAcceptance,HighVisMass,EvtGenCut  
   + 15574085 : Lb_Lc2880munu,D0p,Kmunu=LHCbAcceptance,HighVisMass,EvtGenCut  
   + 15584086 : Lb_Lc2940enu,D0p,Kenu=LHCbAcceptance,HighVisMass,EvtGenCut  
   + 15574099 : Lb_Lc2940enu,D0p,Kmunu=LHCbAcceptance,HighVisMass,EvtGenCut  
   + 15574089 : Lb_Lc2940munu,D0p,Kenu=LHCbAcceptance,HighVisMass,EvtGenCut  
   + 15574086 : Lb_Lc2940munu,D0p,Kmunu=LHCbAcceptance,HighVisMass,EvtGenCut  
  
! 2023-07-11 - Ziyi Wang (MR !1539)  
   Add new decay file  
   + 24104102 : incl_Jpsi,LambdaLambdabar=TightCut,Ttrack  
  
! 2023-07-07 - Anton Poluektov (MR !1537)  
   Add 5 new decay files  
   + 11896090 : Bd_Ds2536D-,DKpi,muX=TightCut  
   + 11898090 : Bd_Ds2536Dst-,DKpi,muX=TightCut  
   + 13576011 : Bs_Ds2536munu,DKpi=ISGW2,TightCut  
   + 12897090 : Bu_Ds2536D0bar,DKpi,muX=TightCut  
   + 12897490 : Bu_Ds2536Dst0bar,DKpi,muX=TightCut  
  
! 2023-07-07 - Dan Thompson (MR !1536)  
   Add 4 new decay files  
   + 15584039 : Lb_D0Lambda1520,KenupK=DecProdCut,HighVisMass,EvtGenCut  
   + 15574088 : Lb_D0Lambda1520,KmunupK=DecProdCut,HighVisMass,EvtGenCut  
   + 15584037 : Lb_D0pK,Kenu=DecProdCut,HighVisMass,EvtGenCut  
   + 15574086 : Lb_D0pK,Kmunu=DecProdCut,HighVisMass,EvtGenCut  
  
! 2023-07-06 - Michele Atzeni (MR !1535)  
   Add new decay file  
   + 27385001 : Dst_D0pi,taue,tauolababar=DecProdCut  
  
! 2023-07-06 - Lakshan Ram Madhan Mohan (MR !1534)  
   Add 4 new decay files  
   + 15524010 : Lb_pKtaue,e=DecProdCut  
   + 15724472 : Lb_pKtaue,pi=TracksInAcc  
   + 15514010 : Lb_pKtaumu,e=DecProdCut  
   + 15714472 : Lb_pKtaumu,pi=TracksInAcc  
  
! 2023-07-06 - Jialu Wang (MR !1533)  
   Add new decay file  
   + 11104145 : Bd_Lambdappi=TightCut  
  
! 2023-07-06 - Jialu Wang (MR !1532)  
   Add new decay file  
   + 11104145 : Bd_Lambdappi=TightCut  
  
! 2023-07-04 - Bogdan Kutsenko (MR !1531)  
   Add 2 new decay files  
   + 11584033 : Bd_Dst+enu,D0pi+=HQET2,TightCut,tighter  
   + 11574095 : Bd_Dst+munu,D0pi+=HQET2,TightCut,tighter  
  
! 2023-07-04 - Fabian Christoph Glaser (MR !1529)  
   Add 10 new decay files  
   + 14643230 : Bc_CharmoniumMuNu,Jpsigamma,ee=BcVegPy,ffWang,TightCut  
   + 14845020 : Bc_psi2SMuNu,Jpsi,ee=BcVegPy,ffKiselev,TightCut  
   + 14543050 : Bc_psi2SMuNu,ee=BcVegPy,ffKiselev,DecProdCut  
   + 11511003 : Bd_Xumunu=TightCut,ForBu2munugamma  
   + 12573210 : Bu_Dst0munu,D0gamma,Kpi=DecProdCut  
   + 12573420 : Bu_Dst0munu,D0pi0,Kpi=DecProdCut  
   + 12711000 : Bu_X0munu=TightCut,ISGW2,ForBu2munugamma  
   + 12511005 : Bu_Xu0munu=TightCut,ForBu2munugamma  
   + 12513200 : Bu_etapmunu,eegamma=TightCut,ISGW2  
   + 12511201 : Bu_gammaMuNuM=DecProdCut,PHSP  
  
! 2023-06-29 - Dmitrii Pereima (MR !1526)  
   Add new decay file  
   + 14175083 : Bc_jpsippbarpi,mm=TightCuts,BC_VPPHAD  
  
! 2023-06-29 - Lorenzo Paolucci (MR !1525)  
   Add new decay file  
   + 12155080 : Bu_psi2SphiK,eeKK=DecProdCut  
  
! 2023-06-24 - Anton Poluektov (MR !1524)  
   Add 10 new decay files  
   + 11896080 : Bd_Ds2460D-,Dspipi,muX=TightCut  
   + 11898080 : Bd_Ds2460Dst-,Dspipi,muX=TightCut  
   + 11896081 : Bd_Ds2536D-,Dspipi,muX=TightCut  
   + 11898081 : Bd_Ds2536Dst-,Dspipi,muX=TightCut  
   + 13676000 : Bs_Ds2460munu,Dspipi=ISGW2,TightCut  
   + 13676001 : Bs_Ds2536munu,Dspipi=ISGW2,TightCut  
   + 12897080 : Bu_Ds2460D0bar,Dspipi,muX=TightCut  
   + 12897480 : Bu_Ds2460Dst0bar,Dspipi,muX=TightCut  
   + 12897081 : Bu_Ds2536D0bar,Dspipi,muX=TightCut  
   + 12897481 : Bu_Ds2536Dst0bar,Dspipi,muX=TightCut  
  
! 2023-06-23 - Saul Lopez Solino (MR !1523)  
   Add 4 new decay files  
   + 17195080 : BstarS2_BplusK,Bplus_DsPiD,D_pipiK=TightCut  
   + 17195081 : BstarS2_BplusK,Bplus_DsstPiD,D_pipiK=TightCut  
   + 17565080 : BstarS2_BplusK,Bplus_TauNuDstar,Dstar_Dpi,D_pipiK=TightCut  
   + 16165100 : Sigmabstar_Lambdab0pi,Lambdab0_Lambda0Dpi,D_pipiK=TightCut  
  
! 2023-06-19 - Zeqing Mu (MR !1503)  
   Add new decay file  
   + 14165090 : Bc_DsPhi,KKpi=DDalitz,KK,BcVegPy,DecProdCut  
  
! 2023-06-16 - Lais Soares Lavra (MR !1502)  
   Add 5 new decay files  
   + 13524000 : Bs_KKtaue,e=DecProdCut  
   + 13514080 : Bs_KKtaue,mu=DecProdCut  
   + 13514090 : Bs_KKtaumu,e=DecProdCut  
   + 16515020 : Sigmab+_Lbpi+,Lb_pKtaumu,mu=DecProdCut  
   + 16515000 : Sigmab_Lbpi,Lb_pKtaumu,mu=DecProdCut  
  
! 2023-05-13 - Michel De Cian (MR !1481)  
   Add 6 new decay files  
   + 13872000 : Bs_DsPi,DsSL=TightCut  
   + 13874030 : Bs_DsPiPiPi,DsSL=TightCut  
   + 13872400 : Bs_DsRhop,DsSL=TightCut  
   + 13872200 : Bs_DsstarPi,DsSL=TightCut  
   + 13874253 : Bs_DsstarPiPiPi,DsSL=TightCut  
   + 13872600 : Bs_DsstarRhop,DsSL=TightCut  
  
! 2023-01-24 - Lisa Fantini (MR !1274)  
   Add 111 new decay files  
   + 14113049 : Bc_EMajoranaNeutrino2MuPiOS,m=1000MeV,t=0ps,DecProdCut  
   + 14113081 : Bc_EMajoranaNeutrino2MuPiOS,m=1000MeV,t=100ps,DecProdCut  
   + 14113050 : Bc_EMajoranaNeutrino2MuPiOS,m=2000MeV,t=0ps,DecProdCut  
   + 14113082 : Bc_EMajoranaNeutrino2MuPiOS,m=2000MeV,t=100ps,DecProdCut  
   + 14113051 : Bc_EMajoranaNeutrino2MuPiOS,m=3000MeV,t=0ps,DecProdCut  
   + 14113083 : Bc_EMajoranaNeutrino2MuPiOS,m=3000MeV,t=100ps,DecProdCut  
   + 14113089 : Bc_EMajoranaNeutrino2MuPiOS,m=3000MeV,t=20ps,DecProdCut  
   + 14113052 : Bc_EMajoranaNeutrino2MuPiOS,m=4000MeV,t=0ps,DecProdCut  
   + 14113084 : Bc_EMajoranaNeutrino2MuPiOS,m=4000MeV,t=100ps,DecProdCut  
   + 14113053 : Bc_EMajoranaNeutrino2MuPiOS,m=5000MeV,t=0ps,DecProdCut  
   + 14113085 : Bc_EMajoranaNeutrino2MuPiOS,m=5000MeV,t=100ps,DecProdCut  
   + 14113048 : Bc_EMajoranaNeutrino2MuPiOS,m=500MeV,t=0ps,DecProdCut  
   + 14113080 : Bc_EMajoranaNeutrino2MuPiOS,m=500MeV,t=100ps,DecProdCut  
   + 14113054 : Bc_EMajoranaNeutrino2MuPiOS,m=5500MeV,t=0ps,DecProdCut  
   + 14113086 : Bc_EMajoranaNeutrino2MuPiOS,m=5500MeV,t=100ps,DecProdCut  
   + 14113055 : Bc_EMajoranaNeutrino2MuPiOS,m=6000MeV,t=0ps,DecProdCut  
   + 14113087 : Bc_EMajoranaNeutrino2MuPiOS,m=6000MeV,t=100ps,DecProdCut  
   + 14113042 : Bc_EMajoranaNeutrino2MuPiSS,m=1000MeV,t=0ps,DecProdCut  
   + 14113073 : Bc_EMajoranaNeutrino2MuPiSS,m=1000MeV,t=100ps,DecProdCut  
   + 14113043 : Bc_EMajoranaNeutrino2MuPiSS,m=2000MeV,t=0ps,DecProdCut  
   + 14113074 : Bc_EMajoranaNeutrino2MuPiSS,m=2000MeV,t=100ps,DecProdCut  
   + 14113075 : Bc_EMajoranaNeutrino2MuPiSS,m=3000MeV,t=100ps,DecProdCut  
   + 14113044 : Bc_EMajoranaNeutrino2MuPiSS,m=4000MeV,t=0ps,DecProdCut  
   + 14113076 : Bc_EMajoranaNeutrino2MuPiSS,m=4000MeV,t=100ps,DecProdCut  
   + 14113045 : Bc_EMajoranaNeutrino2MuPiSS,m=5000MeV,t=0ps,DecProdCut  
   + 14113077 : Bc_EMajoranaNeutrino2MuPiSS,m=5000MeV,t=100ps,DecProdCut  
   + 14113041 : Bc_EMajoranaNeutrino2MuPiSS,m=500MeV,t=0ps,DecProdCut  
   + 14113072 : Bc_EMajoranaNeutrino2MuPiSS,m=500MeV,t=100ps,DecProdCut  
   + 14113046 : Bc_EMajoranaNeutrino2MuPiSS,m=5500MeV,t=0ps,DecProdCut  
   + 14113078 : Bc_EMajoranaNeutrino2MuPiSS,m=5500MeV,t=100ps,DecProdCut  
   + 14113047 : Bc_EMajoranaNeutrino2MuPiSS,m=6000MeV,t=0ps,DecProdCut  
   + 14113079 : Bc_EMajoranaNeutrino2MuPiSS,m=6000MeV,t=100ps,DecProdCut  
   + 14113034 : Bc_MuMajoranaNeutrino2EPiOS,m=1000MeV,t=0ps,DecProdCut  
   + 14113065 : Bc_MuMajoranaNeutrino2EPiOS,m=1000MeV,t=100ps,DecProdCut  
   + 14113035 : Bc_MuMajoranaNeutrino2EPiOS,m=2000MeV,t=0ps,DecProdCut  
   + 14113066 : Bc_MuMajoranaNeutrino2EPiOS,m=2000MeV,t=100ps,DecProdCut  
   + 14113036 : Bc_MuMajoranaNeutrino2EPiOS,m=3000MeV,t=0ps,DecProdCut  
   + 14113067 : Bc_MuMajoranaNeutrino2EPiOS,m=3000MeV,t=100ps,DecProdCut  
   + 14113088 : Bc_MuMajoranaNeutrino2EPiOS,m=3000MeV,t=20ps,DecProdCut  
   + 14113037 : Bc_MuMajoranaNeutrino2EPiOS,m=4000MeV,t=0ps,DecProdCut  
   + 14113068 : Bc_MuMajoranaNeutrino2EPiOS,m=4000MeV,t=100ps,DecProdCut  
   + 14113038 : Bc_MuMajoranaNeutrino2EPiOS,m=5000MeV,t=0ps,DecProdCut  
   + 14113069 : Bc_MuMajoranaNeutrino2EPiOS,m=5000MeV,t=100ps,DecProdCut  
   + 14113033 : Bc_MuMajoranaNeutrino2EPiOS,m=500MeV,t=0ps,DecProdCut  
   + 14113064 : Bc_MuMajoranaNeutrino2EPiOS,m=500MeV,t=100ps,DecProdCut  
   + 14113039 : Bc_MuMajoranaNeutrino2EPiOS,m=5500MeV,t=0ps,DecProdCut  
   + 14113070 : Bc_MuMajoranaNeutrino2EPiOS,m=5500MeV,t=100ps,DecProdCut  
   + 14113040 : Bc_MuMajoranaNeutrino2EPiOS,m=6000MeV,t=0ps,DecProdCut  
   + 14113071 : Bc_MuMajoranaNeutrino2EPiOS,m=6000MeV,t=100ps,DecProdCut  
   + 14113026 : Bc_MuMajoranaNeutrino2EPiSS,m=1000MeV,t=0ps,DecProdCut  
   + 14113057 : Bc_MuMajoranaNeutrino2EPiSS,m=1000MeV,t=100ps,DecProdCut  
   + 14113027 : Bc_MuMajoranaNeutrino2EPiSS,m=2000MeV,t=0ps,DecProdCut  
   + 14113058 : Bc_MuMajoranaNeutrino2EPiSS,m=2000MeV,t=100ps,DecProdCut  
   + 14113059 : Bc_MuMajoranaNeutrino2EPiSS,m=3000MeV,t=100ps,DecProdCut  
   + 14113028 : Bc_MuMajoranaNeutrino2EPiSS,m=4000MeV,t=0ps,DecProdCut  
   + 14113060 : Bc_MuMajoranaNeutrino2EPiSS,m=4000MeV,t=100ps,DecProdCut  
   + 14113029 : Bc_MuMajoranaNeutrino2EPiSS,m=5000MeV,t=0ps,DecProdCut  
   + 14113061 : Bc_MuMajoranaNeutrino2EPiSS,m=5000MeV,t=100ps,DecProdCut  
   + 14113019 : Bc_MuMajoranaNeutrino2EPiSS,m=500MeV,t=0ps,DecProdCut  
   + 14113056 : Bc_MuMajoranaNeutrino2EPiSS,m=500MeV,t=100ps,DecProdCut  
   + 14113030 : Bc_MuMajoranaNeutrino2EPiSS,m=5500MeV,t=0ps,DecProdCut  
   + 14113062 : Bc_MuMajoranaNeutrino2EPiSS,m=5500MeV,t=100ps,DecProdCut  
   + 14113031 : Bc_MuMajoranaNeutrino2EPiSS,m=6000MeV,t=0ps,DecProdCut  
   + 14113063 : Bc_MuMajoranaNeutrino2EPiSS,m=6000MeV,t=100ps,DecProdCut  
   + 12113933 : Bu_EMajoranaNeutrino2MuPiOS,m=1000MeV,t=0ps,DecProdCut  
   + 12113917 : Bu_EMajoranaNeutrino2MuPiOS,m=1000MeV,t=100ps,DecProdCut  
   + 12113934 : Bu_EMajoranaNeutrino2MuPiOS,m=2000MeV,t=0ps,DecProdCut  
   + 12113918 : Bu_EMajoranaNeutrino2MuPiOS,m=2000MeV,t=100ps,DecProdCut  
   + 12113919 : Bu_EMajoranaNeutrino2MuPiOS,m=3000MeV,t=100ps,DecProdCut  
   + 12113935 : Bu_EMajoranaNeutrino2MuPiOS,m=4000MeV,t=0ps,DecProdCut  
   + 12113920 : Bu_EMajoranaNeutrino2MuPiOS,m=4000MeV,t=100ps,DecProdCut  
   + 12113936 : Bu_EMajoranaNeutrino2MuPiOS,m=4500MeV,t=0ps,DecProdCut  
   + 12113921 : Bu_EMajoranaNeutrino2MuPiOS,m=4500MeV,t=100ps,DecProdCut  
   + 12113937 : Bu_EMajoranaNeutrino2MuPiOS,m=5000MeV,t=0ps,DecProdCut  
   + 12113922 : Bu_EMajoranaNeutrino2MuPiOS,m=5000MeV,t=100ps,DecProdCut  
   + 12113068 : Bu_EMajoranaNeutrino2MuPiOS,m=500MeV,t=0ps,DecProdCut  
   + 12113916 : Bu_EMajoranaNeutrino2MuPiOS,m=500MeV,t=100ps,DecProdCut  
   + 12113929 : Bu_EMajoranaNeutrino2MuPiSS,m=1000MeV,t=0ps,DecProdCut  
   + 12113910 : Bu_EMajoranaNeutrino2MuPiSS,m=1000MeV,t=100ps,DecProdCut  
   + 12113930 : Bu_EMajoranaNeutrino2MuPiSS,m=2000MeV,t=0ps,DecProdCut  
   + 12113911 : Bu_EMajoranaNeutrino2MuPiSS,m=2000MeV,t=100ps,DecProdCut  
   + 12113912 : Bu_EMajoranaNeutrino2MuPiSS,m=3000MeV,t=100ps,DecProdCut  
   + 12113931 : Bu_EMajoranaNeutrino2MuPiSS,m=4000MeV,t=0ps,DecProdCut  
   + 12113913 : Bu_EMajoranaNeutrino2MuPiSS,m=4000MeV,t=100ps,DecProdCut  
   + 12113932 : Bu_EMajoranaNeutrino2MuPiSS,m=4500MeV,t=0ps,DecProdCut  
   + 12113914 : Bu_EMajoranaNeutrino2MuPiSS,m=4500MeV,t=100ps,DecProdCut  
   + 12113067 : Bu_EMajoranaNeutrino2MuPiSS,m=5000MeV,t=0ps,DecProdCut  
   + 12113915 : Bu_EMajoranaNeutrino2MuPiSS,m=5000MeV,t=100ps,DecProdCut  
   + 12113928 : Bu_EMajoranaNeutrino2MuPiSS,m=500MeV,t=0ps,DecProdCut  
   + 12113909 : Bu_EMajoranaNeutrino2MuPiSS,m=500MeV,t=100ps,DecProdCut  
   + 12113923 : Bu_MuMajoranaNeutrino2EPiOS,m=1000MeV,t=0ps,DecProdCut  
   + 12113924 : Bu_MuMajoranaNeutrino2EPiOS,m=2000MeV,t=0ps,DecProdCut  
   + 12113925 : Bu_MuMajoranaNeutrino2EPiOS,m=4000MeV,t=0ps,DecProdCut  
   + 12113926 : Bu_MuMajoranaNeutrino2EPiOS,m=4500MeV,t=0ps,DecProdCut  
   + 12113907 : Bu_MuMajoranaNeutrino2EPiOS,m=4500MeV,t=100ps,DecProdCut  
   + 12113927 : Bu_MuMajoranaNeutrino2EPiOS,m=5000MeV,t=0ps,DecProdCut  
   + 12113908 : Bu_MuMajoranaNeutrino2EPiOS,m=5000MeV,t=100ps,DecProdCut  
   + 12113044 : Bu_MuMajoranaNeutrino2EPiOS,m=500MeV,t=0ps,DecProdCut  
   + 12113028 : Bu_MuMajoranaNeutrino2EPiSS,m=1000MeV,t=0ps,DecProdCut  
   + 12113901 : Bu_MuMajoranaNeutrino2EPiSS,m=1000MeV,t=100ps,DecProdCut  
   + 12113029 : Bu_MuMajoranaNeutrino2EPiSS,m=2000MeV,t=0ps,DecProdCut  
   + 12113902 : Bu_MuMajoranaNeutrino2EPiSS,m=2000MeV,t=100ps,DecProdCut  
   + 12113903 : Bu_MuMajoranaNeutrino2EPiSS,m=3000MeV,t=100ps,DecProdCut  
   + 12113030 : Bu_MuMajoranaNeutrino2EPiSS,m=4000MeV,t=0ps,DecProdCut  
   + 12113904 : Bu_MuMajoranaNeutrino2EPiSS,m=4000MeV,t=100ps,DecProdCut  
   + 12113042 : Bu_MuMajoranaNeutrino2EPiSS,m=4500MeV,t=0ps,DecProdCut  
   + 12113905 : Bu_MuMajoranaNeutrino2EPiSS,m=4500MeV,t=100ps,DecProdCut  
   + 12113043 : Bu_MuMajoranaNeutrino2EPiSS,m=5000MeV,t=0ps,DecProdCut  
   + 12113906 : Bu_MuMajoranaNeutrino2EPiSS,m=5000MeV,t=100ps,DecProdCut  
   + 12113027 : Bu_MuMajoranaNeutrino2EPiSS,m=500MeV,t=0ps,DecProdCut  
   + 12113900 : Bu_MuMajoranaNeutrino2EPiSS,m=500MeV,t=100ps,DecProdCut  
  
