DecFiles v30r76 2022-07-14 
==========================  
 
! 2022-07-13 - Niladri Sahoo (MR !1106)  
   Add 2 new decay files  
   + 14175051 : Bc_Dsst2573mumu,KKpi=BcVegPy,DecProdCut  
   + 14175052 : Bc_JpsiDsst2573,mmKKpi=BcVegPy,DecProdCut  
  
! 2022-07-05 - Yanxi Zhang (MR !1105)  
   Add new decay file  
   + 15266100 : Lb_LambdaDspi,ppi,KKpi,TightCut  
  
! 2022-07-04 - Arnau Brossa Gonzalo (MR !1104)  
   Modify decay file  
   + 23203491 : Ds+_pipipipi0,gg=Cocktail,TightCut  
  
! 2022-07-01 - Francesco Dettori (MR !1103)  
   Add 2 new decay files  
   + 11112207 : Bd_gammamumu=ISR,MassCut4  
   + 13112206 : Bs_gammamumu=ISR,MassCut4  
  
! 2022-07-01 - Menglin Xu (MR !1102)  
   Add new decay file  
   + 42912000 : ZZ_l,X=1l15GeV  
  
! 2022-06-30 - Michel De Cian (MR !1101)  
   Add new decay file  
   + 12513005 : Bu_f0_500munu,PiPi=TightCut,ISGW2  
  
! 2022-06-25 - Gary Robertson (MR !1100)  
   Add 9 new decay files  
   + 26196047 : Pc4350,LcpiD0,pkpi=TightCut,InAcc  
   + 26196048 : Pc4500,LcpiD0,pkpi=TightCut,InAcc  
   + 26197975 : Pc4550,LcpiDst+,pkpi=TightCut,InAcc  
   + 26196049 : Pc4650,LcpiD0,pkpi=TightCut,InAcc  
   + 26197976 : Pc4700,LcpiDst+,pkpi=TightCut,InAcc  
   + 26197977 : Pc4850,LcpiDst+,pkpi=TightCut,InAcc  
   + 26196941 : Pcc4600,LcD+,pkpi=TightCut,InAcc  
   + 26195072 : Pcc4600,LcD0,pkpi=TightCut,InAcc  
   + 26196942 : Pcc4800,LcDst+,pkpi=TightCut,InAcc  
  
