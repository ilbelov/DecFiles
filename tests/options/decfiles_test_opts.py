from Configurables import GenInit, LHCbApp

GenInit('GaussGen', FirstEventNumber=1, RunNumber=1082)
LHCbApp(EvtMax=5)
