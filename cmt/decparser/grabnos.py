#!/usr/bin/env python

from __future__ import print_function

import sys
import json
import urllib
if sys.version_info[0] > 2:
    import urllib.request
import settings

def cleanTags(name):
    while "<" in name or ">" in name:
        i = name.find("<")
        j = name.find(">")
        name = name[:i]+name[j+1:]
    return name

def grabnos():
    handle = None
    if sys.version_info[0] < 3:
        handle = urllib.urlopen(settings.dec_url)
    else:
        handle = urllib.request.urlopen(settings.dec_url)

    nosstarted = False
    nextno = False
    noinprogress = False
    id = []
    names = []
    k=0

    urloutput = handle.read().decode('iso-8859-1')
    for line in urloutput.split('\n'):
        k+=1
        if "<tr class=titlerow>" in line:
            nosstarted = True
            continue
        if "</tr>" in line and nosstarted:
            noinprogress = False
            nextno = True
            continue
        if "<tr class=tyesrow>" in line and nextno:
            noinprogress = True
            nextno = False
            continue
        if "http://lhcbdoc.web.cern.ch/lhcbdoc/decfiles/releases/" in line and noinprogress:

            id += [int(line.partition("</a>")[0].partition("py>")[2])]
        if "https://gitlab.cern.ch/lhcb-datapkg/Gen/DecFiles/tree/" in line and noinprogress:
            line = line.partition("<b>")[2]
            while not line.find("<br>") == -1:
                name = line.partition("<br>")[0].strip()
                names +=[cleanTags(name)]
                line = line.partition("<br>")[2]

    return zip(id,names)

def grabobs():
    handle = None
    if sys.version_info[0] < 3:
        handle = urllib.urlopen(settings.obs_url)
    else:
        handle = urllib.request.urlopen(settings.obs_url)
    obsnos = []

    urloutput = handle.read().decode('iso-8859-1')
    for line in urloutput.split('n'):
        obsnos+=[line.partition("EVTTYPEID = ")[2].partition(", DESCRIPTION ")[0]]
    return obsnos

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def grabcuts():
    cuts = []
    revs = []

    handle = None
    if sys.version_info[0] < 3:
        handle = urllib.urlopen(settings.cuts_url)
        sourcecode = handle.read()
    else:
        handle = urllib.request.urlopen(settings.cuts_url)
        sourcecode = handle.read().decode()
 
    result = json.loads(sourcecode)
    for line in result:
        if '.cpp' in line['name']:
            cutName = line['name'].split('.')[0]
            cuts.append(cutName)
 
    return cuts

def grabIdToName():
    dict = {}

    handle = None
    if sys.version_info[0] < 3:
        handle = urllib.urlopen(settings.table_url)
    else:
        handle = urllib.request.urlopen(settings.table_url)

    skip = True
    urloutput = handle.read().decode('iso-8859-1')
    for line in urloutput.split('\n'):
        if skip:
            skip = False
            continue
        if "end" in line:
            break
        if line[0]=='*':
            continue
        line = line.split()
        dict[line[4]] = line[3]

    return dict

if __name__ == "__main__":
    nos = grabnos()
    for no in nos:
        print(no)
    print(grabobs())
    print(grabcuts())
    print(grabIdToName())
