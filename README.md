# Deadline for next release

We aim to produce new releases for Sim09 and Sim10 on Fridays every two weeks. 
However, this can change depending on the availability of the maintainers and the urgency of the requests.  

# Guide for submitting decay file
Please follow [Contribution guide](https://gitlab.cern.ch/lhcb-datapkg/Gen/DecFiles/blob/master/CONTRIBUTING.md) on top of the page for instructions on how to prepare, test and commit decay file.

# Overiew of existing decay files
A list of existing decay files, event numbers, and related comments can be found here: [http://lhcbdoc.web.cern.ch/lhcbdoc/decfiles/](http://lhcbdoc.web.cern.ch/lhcbdoc/decfiles/).
